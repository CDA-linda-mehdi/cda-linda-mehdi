package com.afpa.dao;

import java.util.List;

import com.afpa.entity.Voiture;

public interface IVoitureDao extends IDao<Voiture> {

	Voiture getVoitureByMatricule(String matricule);

	List<Voiture> findVoituresByColorLabel(String couleur);
	
	//List<Voiture> findVoituresByEnergieLabel(String energie);

}
