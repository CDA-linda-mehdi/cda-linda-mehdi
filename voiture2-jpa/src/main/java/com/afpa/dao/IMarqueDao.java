package com.afpa.dao;

import com.afpa.entity.Marque;

public interface IMarqueDao extends IDao<Marque> {

	Marque getMarqueByLabel(String marque);

}
