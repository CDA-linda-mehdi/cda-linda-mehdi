package com.afpa.dao.impl;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import com.afpa.dao.ICouleurDao;
import com.afpa.entity.Couleur;

public class CouleurDaoImpl extends AbstractDao<Couleur> implements ICouleurDao {

	@Override
	public Couleur getCouleurByLabel(String couleur) {
		EntityManager em = null;
		try {
			em = newEntityManager();
			TypedQuery<Couleur> q = em.createNamedQuery("getCouleurByLabel", Couleur.class);
			q.setParameter("labelParam", couleur);
			return q.getSingleResult();
		}catch(NoResultException e) {
			return null;
		} finally {
			closeEntityManager(em);
		}
	}
}
