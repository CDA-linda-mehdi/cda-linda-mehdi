package com.afpa.dao;

import com.afpa.entity.Modele;

public interface IModeleDao extends IDao<Modele> {

	Modele getModeleByLabel(String modele);

}
