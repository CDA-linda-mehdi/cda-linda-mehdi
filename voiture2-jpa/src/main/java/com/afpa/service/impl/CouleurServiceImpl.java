package com.afpa.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import com.afpa.dao.ICouleurDao;
import com.afpa.dao.IVoitureDao;
import com.afpa.dao.impl.CouleurDaoImpl;
import com.afpa.dao.impl.VoitureDaoImlp;
import com.afpa.dto.reponse.CreationReponseDto;
import com.afpa.dto.reponse.ElementSimpleDto;
import com.afpa.dto.reponse.ElementsListeDto;
import com.afpa.dto.reponse.ReponseDto;
import com.afpa.dto.reponse.Status;
import com.afpa.entity.Couleur;
import com.afpa.entity.Voiture;
import com.afpa.service.ICouleurService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CouleurServiceImpl implements ICouleurService {

	private final ICouleurDao couleurDao;
	private final IVoitureDao voitureDao;

	public CouleurServiceImpl() {
		this.couleurDao = new CouleurDaoImpl();
		this.voitureDao = new VoitureDaoImlp();
	}

	@Override
	public ReponseDto creerCouleur(String couleur) {
		Couleur c = couleurDao.getCouleurByLabel(couleur);
		if (c != null) {
			return ReponseDto.builder().code(Status.KO).msg("erreur : cette couleur existe d�j�").build();
		}
		c = couleurDao.add(Couleur.builder().label(couleur).build());
		return ReponseDto.builder()
				.code(Status.OK)
				.msg("creation de la couleur code " + c.getCode() + " ok!")
				.contenu(CreationReponseDto.builder().code(c.getCode()).build())
				.build();
	}

	@Override
	public ReponseDto chercherCouleurParLabel(String couleur) {
		Couleur c = couleurDao.getCouleurByLabel(couleur);
		if (c == null) {
			return ReponseDto.builder().code(Status.KO).msg("erreur : cette couleur n'existe pas").build();
		}
		return ReponseDto.builder()
				.code(Status.OK)
				.msg("couleur trouv�e")
				.contenu(CreationReponseDto.builder().code(c.getCode()).build())
				.build();
	}
	
	@Override
	public ReponseDto chercherCouleurParCode(int code) {
		Couleur c = couleurDao.find(code);
		if (c == null) {
			return ReponseDto.builder().code(Status.KO).msg("erreur : aucune couleur n'a ce code : "+code).build();
		}
		return ReponseDto.builder()
				.code(Status.OK)
				.msg("une couleur est trouv�e")
				.contenu(
						ElementSimpleDto.builder()
						.code(c.getCode())
						.label(c.getLabel())
						.build()
						)
				.build();
	}

	@Override
	public ReponseDto recupererToutesLesCouleurs() {
		List<Couleur> couleursEntities = this.couleurDao.findAllByNamedQuery("Couleur.findAll");
		
		List<ElementSimpleDto> res = couleursEntities.stream()
			.map(x->ElementSimpleDto.builder()
					.label(x.getLabel())
					.code(x.getCode())
					.build())
			.collect(Collectors.toList());
		
		return ReponseDto.builder()
				.code(Status.OK)
				.contenu(ElementsListeDto.builder().elements(res).build())
				.build();
	}

	@Override
	public ReponseDto mettreAjour(String couleur, String nouveauLabel) {
		Couleur c = this.couleurDao.getCouleurByLabel(nouveauLabel);
		if (c != null) {
			return ReponseDto.builder().code(Status.KO).msg("erreur : la couleur "+nouveauLabel+" existe deja code : "+c.getCode()).build();
		}
		
		c = this.couleurDao.getCouleurByLabel(couleur);
		if (c == null) {
			log.error("couleur n'existe pas label : "+couleur);
			return ReponseDto.builder().code(Status.KO).msg("erreur : cette couleur n'existe pas").build();
		}
		
		c.setLabel(nouveauLabel);
		
		this.couleurDao.update(c);
		
		return ReponseDto.builder()
				.code(Status.OK)
				.msg("mise � jour r�ussie")
				.contenu(ElementSimpleDto
						.builder()
						.code(c.getCode())
						.label(c.getLabel())
						.build())
				.build();
	}

	@Override
	public ReponseDto supprimerCouleur(String couleur) {
		Couleur c = this.couleurDao.getCouleurByLabel(couleur);
		if (c == null) {
			return ReponseDto.builder().code(Status.KO).msg("erreur : la couleur "+couleur+" n'existe deja ").build();
		}
		
		List<Voiture> voitures = this.voitureDao.findVoituresByColorLabel(couleur);
		if (! (voitures == null || voitures.isEmpty())) {
			return ReponseDto.builder().code(Status.KO).msg("erreur : des voitures ont cette couleur exemple : "+voitures.get(0).getMatricule()).build();
		}
		
		this.couleurDao.remove(c.getCode());
		
		return ReponseDto.builder()
				.code(Status.OK)
				.msg("suppression ok")
				.contenu(ElementSimpleDto
						.builder()
						.code(c.getCode())
						.label(c.getLabel())
						.build())
				.build();
	}

}
