package com.afpa.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import com.afpa.dao.IMarqueDao;
import com.afpa.dao.impl.MarqueDaoImlp;
import com.afpa.dto.reponse.CreationReponseDto;
import com.afpa.dto.reponse.ElementSimpleDto;
import com.afpa.dto.reponse.ElementsListeDto;
import com.afpa.dto.reponse.ReponseDto;
import com.afpa.dto.reponse.Status;
import com.afpa.entity.Marque;
import com.afpa.service.IMarqueService;

public class MarqueServiceImpl implements IMarqueService {

	private final IMarqueDao marqueDao;
	
	public MarqueServiceImpl() {
		this.marqueDao = new MarqueDaoImlp();
	}
	
	public ReponseDto creerMarque(String marque) {
		Marque m = this.marqueDao.getMarqueByLabel(marque);
		if(m != null) {
			return ReponseDto.builder().code(Status.KO).msg("erreur : cette marque existe d�j�").build();
		}
		m = this.marqueDao.add(Marque.builder().label(marque).build());
		return ReponseDto.builder()
				.code(Status.OK)
				.msg("creation de la marque code "+m.getCode()+" ok!")
				.contenu(CreationReponseDto.builder().code(m.getCode()).build())
				.build();
	}

	@Override
	public ReponseDto chercherMarqueParCode(int code) {
		Marque c = this.marqueDao.find(code);
		if (c == null) {
			return ReponseDto.builder().code(Status.KO).msg("erreur : aucune marque n'a ce code").build();
		}
		return ReponseDto.builder()
				.code(Status.OK)
				.msg("une marque est trouv�e")
				.contenu(
						ElementSimpleDto.builder()
						.code(c.getCode())
						.label(c.getLabel())
						.build()
						)
				.build();
	}
	
	@Override
	public ReponseDto chercherMarqueParLabel(String marque) {
		Marque m = this.marqueDao.getMarqueByLabel(marque);
		if (m == null) {
			return ReponseDto.builder().code(Status.KO).msg("erreur : cette marque n'existe pas").build();
		}
		return ReponseDto.builder()
				.code(Status.OK)
				.msg("marque trouv�e")
				.contenu(CreationReponseDto.builder().code(m.getCode()).build())
				.build();
	}

	@Override
	public ReponseDto recupererToutesLesMarques() {
		List<Marque> marquesEntities = this.marqueDao.findAllByNamedQuery("Marque.findAll");
		
		List<ElementSimpleDto> res = marquesEntities.stream()
			.map(x->ElementSimpleDto.builder()
					.label(x.getLabel())
					.code(x.getCode())
					.build())
			.collect(Collectors.toList());
		
		return ReponseDto.builder()
				.code(Status.OK)
				.contenu(ElementsListeDto.builder().elements(res).build())
				.build();
	}
	
}
