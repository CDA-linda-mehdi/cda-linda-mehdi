package com.afpa.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import com.afpa.dao.IEnergieDao;
import com.afpa.dao.IVoitureDao;
import com.afpa.dao.impl.EnergieDaoImpl;
import com.afpa.dao.impl.VoitureDaoImlp;
import com.afpa.dto.reponse.CreationReponseDto;
import com.afpa.dto.reponse.ElementSimpleDto;
import com.afpa.dto.reponse.ElementsListeDto;
import com.afpa.dto.reponse.ReponseDto;
import com.afpa.dto.reponse.Status;
import com.afpa.entity.Energie;
import com.afpa.entity.Voiture;
import com.afpa.service.IEnergieService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class EnergieServiceImpl implements IEnergieService {
	
	private final IEnergieDao energieDao;
	private final IVoitureDao voitureDao;

	public EnergieServiceImpl() {
		this.energieDao = new EnergieDaoImpl();
		this.voitureDao = new VoitureDaoImlp();
	}

	@Override
	public ReponseDto creerEnergie(String energie) {
		Energie e = this.energieDao.getEnergieByLabel(energie);
		if (e != null) {
			return ReponseDto.builder().code(Status.KO).msg("erreur : cette energie existe d�j�").build();
		}
		e = this.energieDao.add(Energie.builder().label(energie).build());
		return ReponseDto.builder()
				.code(Status.OK)
				.msg("creation de l'energie code " + e.getCode() + " ok!")
				.contenu(CreationReponseDto.builder().code(e.getCode()).build())
				.build();
	}
	
	@Override
	public ReponseDto chercherEnergieParCode(int code) {
		Energie c = this.energieDao.find(code);
		if (c == null) {
			return ReponseDto.builder().code(Status.KO).msg("erreur : aucune energie n'a ce code").build();
		}
		return ReponseDto.builder()
				.code(Status.OK)
				.msg("une energie est trouv�e")
				.contenu(
						ElementSimpleDto.builder()
						.code(c.getCode())
						.label(c.getLabel())
						.build()
						)
				.build();
	}
	
	@Override
	public ReponseDto chercherEnergieParLabel(String energie) {
		Energie e = this.energieDao.getEnergieByLabel(energie);
		if (e == null) {
			return ReponseDto.builder().code(Status.KO).msg("erreur : cette energie n'existe pas").build();
		}
		return ReponseDto.builder()
				.code(Status.OK)
				.msg("energie trouv�e")
				.contenu(CreationReponseDto.builder().code(e.getCode()).build())
				.build();
	}

	@Override
	public ReponseDto recupererToutesLesEnergies() {
		List<Energie> marquesEntities = this.energieDao.findAllByNamedQuery("Energie.findAll");
		
		List<ElementSimpleDto> res = marquesEntities.stream()
			.map(x->ElementSimpleDto.builder()
					.label(x.getLabel())
					.code(x.getCode())
					.build())
			.collect(Collectors.toList());
		
		return ReponseDto.builder()
				.code(Status.OK)
				.contenu(ElementsListeDto.builder().elements(res).build())
				.build();
	}

	@Override
	public ReponseDto mettreAjour(String energie, String nouveauLabel) {
		Energie e = this.energieDao.getEnergieByLabel(nouveauLabel);
		if (e != null) {
			return ReponseDto.builder().code(Status.KO).msg("erreur : l'energie " +nouveauLabel+ " existe d�j� code : " +e.getCode()).build();	
		}
		
		e = this.energieDao.getEnergieByLabel(energie);
		if (e == null) {
			log.error("energie n'existe pas label : " +energie);
			return ReponseDto.builder().code(Status.KO).msg("erreur : cette couleur n'existe pas").build();
		}
		e.setLabel(nouveauLabel);
		
		this.energieDao.update(e);
		
		return ReponseDto.builder()
				.code(Status.OK)
				.msg("mise � jour r�ussie")
				.contenu(ElementSimpleDto.builder()
						.code(e.getCode())
						.label(e.getLabel())
						.build())
				.build();
	}


//	@Override
//	public ReponseDto supprimerEnergie(String energie) {
//		Energie e = this.energieDao.getEnergieByLabel(energie);
//		if (e == null) {
//			return ReponseDto.builder().code(Status.KO).msg("erreur : l'�nergie " +energie+ " n'existe pas").build();
//		}
//		
//		List<Voiture> voitures = this.voitureDao.findVoituresByEnergieLabel(energie);
//		if (! (voitures == null || voitures.isEmpty())) {
//			return ReponseDto.builder().code(Status.KO).msg("erreur : des voitures ont cette �nergie exemple : " +voitures.get(0).getMatricule()).build();
//		}
//		
//		this.energieDao.remove(e.getCode());
//		
//		return ReponseDto.builder()
//				.code(Status.OK)
//				.msg("suppression ok")
//				.contenu(ElementSimpleDto.builder()
//						.code(e.getCode())
//						.label(e.getLabel())
//						.build())
//				.build();
//	}
}
