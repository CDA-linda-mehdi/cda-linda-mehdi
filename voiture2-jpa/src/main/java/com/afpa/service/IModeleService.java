package com.afpa.service;

import com.afpa.dto.reponse.ReponseDto;
import com.afpa.dto.requete.ModeleCreationDto;

public interface IModeleService {

	ReponseDto creerModele(ModeleCreationDto creationDto);

	ReponseDto chercherModeleParLabel(String mODELE_M6_LABEL);

	ReponseDto recupererTousLesModeles();

}
