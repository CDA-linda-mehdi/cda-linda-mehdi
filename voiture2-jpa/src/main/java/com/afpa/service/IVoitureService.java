package com.afpa.service;

import com.afpa.dto.reponse.ReponseDto;
import com.afpa.dto.requete.VoitureCreationDto;

public interface IVoitureService {

	ReponseDto creerVoiture(VoitureCreationDto build);

	ReponseDto recupererToutesLesVoitures();

}
