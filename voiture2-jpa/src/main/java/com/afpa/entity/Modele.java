package com.afpa.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@NamedQueries({
	@NamedQuery(name = "getModeleByLabel",query = "select m from Modele m where UPPER(m.label)=UPPER( :labelParam)"),
	@NamedQuery(name = "Modele.findAll",query = "select m from Modele m")
})
@Table(name = "t_modele", 
	uniqueConstraints = { 
			@UniqueConstraint(columnNames = { "label" }) 
		})
public class Modele {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int code;

	@Column(nullable = false)
	private String label;

	@Column(nullable = false)
	private int puissance;

	@ManyToOne
	private Energie energie;

	@ManyToOne
	private Marque marque;
}
