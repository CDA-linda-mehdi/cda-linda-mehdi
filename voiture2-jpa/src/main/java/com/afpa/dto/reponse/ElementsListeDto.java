package com.afpa.dto.reponse;

import java.util.ArrayList;
import java.util.List;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class ElementsListeDto implements IContenuDto {
	@Builder.Default
	List<? extends IContenuDto> elements = new ArrayList<>();
}
