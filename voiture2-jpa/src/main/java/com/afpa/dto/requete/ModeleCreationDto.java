package com.afpa.dto.requete;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class ModeleCreationDto {
	private String modele;
	private String marque;
	private int puissance;
	private String energie;
}
