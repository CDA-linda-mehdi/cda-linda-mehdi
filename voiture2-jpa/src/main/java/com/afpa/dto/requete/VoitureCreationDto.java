package com.afpa.dto.requete;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class VoitureCreationDto {
	private String modele;
	private String couleur;
	private String matricule;
}
