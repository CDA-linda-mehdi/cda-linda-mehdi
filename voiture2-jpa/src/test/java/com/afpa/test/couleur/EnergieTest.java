package com.afpa.test.couleur;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.afpa.dto.reponse.CreationReponseDto;
import com.afpa.dto.reponse.ReponseDto;
import com.afpa.dto.reponse.Status;
import com.afpa.service.IEnergieService;
import com.afpa.service.impl.EnergieServiceImpl;

public class EnergieTest {
	
	static IEnergieService energieService;
	
	@BeforeAll
	public static void init() {
		energieService = new EnergieServiceImpl();
	}
	
	static int idEnergieCree;
	
	@Test
	void creation() {
		ReponseDto reponse = energieService.creerEnergie("diesel"+System.currentTimeMillis());
		assertNotNull(reponse);
		assertEquals(reponse.getCode(), Status.OK);
		assertNotNull(reponse.getContenu());
		idEnergieCree =((CreationReponseDto) reponse.getContenu()).getCode();
	}
	
	
	
}
