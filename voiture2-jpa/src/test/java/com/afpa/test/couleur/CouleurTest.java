package com.afpa.test.couleur;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import com.afpa.dto.reponse.CreationReponseDto;
import com.afpa.dto.reponse.ElementSimpleDto;
import com.afpa.dto.reponse.ReponseDto;
import com.afpa.dto.reponse.Status;
import com.afpa.service.ICouleurService;
import com.afpa.service.impl.CouleurServiceImpl;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@TestMethodOrder(OrderAnnotation.class)
public class CouleurTest {
	
	static ICouleurService couleurService;
	
	@BeforeAll
	public static void init() {
		couleurService = new CouleurServiceImpl();
	}
	
	static int couleurCreeCode;
	static String couleurCreeLabel;
	
	@Test
	@Order(1)
	void creation() {
		couleurCreeLabel = "rouge"+System.currentTimeMillis();
		ReponseDto reponse = couleurService.creerCouleur(couleurCreeLabel);
		log.debug(reponse.toString());
		assertNotNull(reponse);
		assertEquals(reponse.getCode(), Status.OK);
		assertNotNull(reponse.getContenu());
		couleurCreeCode =((CreationReponseDto) reponse.getContenu()).getCode();
	}
	
	@Test
	@Order(2)
	void recherche() {
		ReponseDto reponse = couleurService.chercherCouleurParCode(couleurCreeCode);
		log.debug(reponse.toString());
		assertNotNull(reponse);
		assertEquals(Status.OK,reponse.getCode());
		assertNotNull(reponse.getContenu());
//		idCouleurCree =((CreationReponseDto) reponse.getContenu()).getId();
	}
	
	@Test
	@Order(3)
	void modificatio() {
		String nouveauLabel = "rouge"+System.currentTimeMillis()+"M";
		ReponseDto reponse = couleurService.mettreAjour(couleurCreeLabel,nouveauLabel);
		assertNotNull(reponse);
		assertEquals(Status.OK,reponse.getCode());
		assertNotNull(reponse.getContenu());
		ElementSimpleDto resDto = (ElementSimpleDto)reponse.getContenu();
		assertEquals(nouveauLabel,resDto.getLabel());
	}
	
}
