package P.Exo4;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Point {

	public Point(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}
	
	private void affiche() {
		System.out.println(" [ " + x + ", " + y + "] ");
		
	}

	private int x, y;

	public static void main(String[] args) {
		Point[] pointsTab = {new Point(-3, 4), new Point(6,-3), new Point(2, 3)};
//		Stream<Point> streamPoints = Arrays.stream(pointsTab);
//
//		Stream<Point> streamPointPositifs = streamPoints.filter(p -> p.getX() > 0);
//
//		List<Point> pointsPostifsList = streamPointPositifs.collect(Collectors.toList());
//
//		for (Point p : pointsPostifsList) {
//			p.affiche();
//		}
//		
//		for (Point p : pointsPostifsList) {
//			p.affiche();
//		}
		
		
		//OU
//		List<Point> pointsPostifsList = Arrays.stream(pointsTab)
//				.filter(p -> p.getX() > 0)
//				.collect(Collectors.toList());
//
//		for (Point p : pointsPostifsList) {
//			p.affiche();
//		}
		
		//OU 
		Arrays.stream(pointsTab)
		.filter(p->p.getX()>0)
		.forEach(p->p.affiche());
		
	}
}