package P.exo2;

public class Affichage {

	public static void main(String[] args) {
		int [] tab = {1, 4, -2, 9, -3, 5, -3, 12, 7, 6, -11, 0} ;

        System.out.print ("-- Les positifs de tab : ") ;
        affichage_selectif (tab, ee -> ee > 0) ;
        System.out.print ("\n-- Les n�gatifs de tab : ") ;
        affichage_selectif (tab, ee -> ee < 0) ;
        System.out.print ("\n-- Les nombres pairs de tab : ") ;
        affichage_selectif (tab, ee -> ee %2 == 0) ;
	}

	private static void affichage_selectif(int[] tab, IMonInterface monTraitement) {
		for (int e : tab) {
			if(monTraitement.Methode(e)) {
				System.out.print(" * "+e);
			}
		}
	}

}
