package P.Exo2CorrectionClement;

import java.util.function.Predicate;

public class Affichage {
    public static void main(String[] args) {
        int[] tab = { 1, 4, -2, 9, -3, 5, -3 };
        System.out.println("-- Les positifs de tab : ");
        affichage_selectif(tab, ee -> ee > 0);
        System.out.println("ma methode");
        affichage_selectif(tab, Interfaceclem::positif);
        System.out.println("-- Les negatifs de tab : ");
        affichage_selectif(tab, ee -> ee < 0);
        System.out.println("ma methode");
        affichage_selectif(tab, Interfaceclem::negatif);
        System.out.println("-- Les pairs de tab : ");
        affichage_selectif(tab, ee -> ee % 2 == 0);
        System.out.println("ma methode");
        affichage_selectif(tab, Interfaceclem::pair);
    }
    
    private static void affichage_selectif2(int[] tab, Predicate<Integer> b) {
        for (int i : tab) {
            if (b.test(i)) {
                System.out.println(i);
            }
        }
    }
    private static void affichage_selectif(int[] tab, Interfaceclem b) {
        for (int i : tab) {
            if (b.clement(i)) {
                System.out.println(i);
            }
        }
    }
}