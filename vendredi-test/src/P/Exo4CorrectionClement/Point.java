package P.Exo4CorrectionClement;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;
public class Point {
    private int x, y;
    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }
    public int getX() {
        return x;
    }
    public int getY() {
        return y;
    }
    public void setX(int x) {
        this.x = x;
    }
    public void setY(int y) {
        this.y = y;
    }
    public void affiche() {
        System.out.print(" [ " + x + ", " + y + "] ");
    }
    @Override
    public String toString() {
        return "Point [x=" + x + ", y=" + y + "]";
    }
    public static void main(String[] args) {
         exo1();
        // exo2();
        // exo3();
        // exo4();
    }
    public static void exo4() {
        String[] motsTab = { "aaaaa", "abrrrbb", "dCCCCCCC", "d", "jkl" };
        Map<String, List<String>> motsMap = Arrays.stream(motsTab)
                .collect(Collectors.groupingBy(p -> p.substring(0, 1)));
        System.out.println(motsMap);
    }
    public static void exo3() {
        String[] motsTab = { "aaaaa", "brrrbb", "CCCCCCC", "d" };
        String resultat = Arrays.stream(motsTab).filter(m -> m.length() > 4).collect(Collectors.joining("|"));
        System.out.println(resultat);
    }
    public static void exo2() {
        Integer[] entiersTab = { 4, 5, -6, 4, 8 };
        Random r = new Random();
        Stream<Point> pointsStream = Arrays.stream(entiersTab).map(e -> new Point(e, r.nextInt(10)));
        Map<Integer, List<Point>> pointsMap = pointsStream.collect(Collectors.groupingBy(x -> x.getX()));
        System.out.println(pointsMap);
    }
    public static void exo1() {
        Point[] pointsTab = { new Point(-2, 5), new Point(7, 3), new Point(6, -3), new Point(-6, 1), new Point(8, 5) };
        List<Point> pointsList = Arrays.stream(pointsTab).filter(p -> p.getX() > 0).collect(Collectors.toList());
        System.out.println(pointsList);
    }
}
