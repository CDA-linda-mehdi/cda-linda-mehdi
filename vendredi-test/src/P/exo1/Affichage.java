package P.exo1;

import java.util.function.Predicate;

public class Affichage {

	public static void main(String[] args) {
		  int [] tab = {1, 4, -2, 9, -3, 5, -3 } ;
	        System.out.print ("-- Les positifs de tab : ") ;
	        
	        affichage_selectif (tab, ee -> ee > 0);
	        
	        affichage_selectif (tab, ee -> {return ee > 0;});
	        
	        Predicate<Integer> p1 = epp -> epp > 0;
	        Predicate<Integer> p2 = epp -> epp %2 == 0; 
	        
	        affichage_selectif2(tab, p1.and(p2));
	}

	private static void affichage_selectif(int[] tab, IMonInterface monTraitement) {
		for (int e : tab) {
			if(monTraitement.Methode(e)) {
				System.out.println(e);
			}
		}
		
	}

	private static void affichage_selectif2(int[] tab, Predicate<Integer> monTraitement) {
		for (int e : tab) {
			if(monTraitement.test(e)) {
				System.out.println(e);
			}
		}
		
	}
}
