package cda.http.server;

public enum CdaStatus {
	
	OK(200,"CDA OK");
	
	private final int code;

	private final String description;
	
	private CdaStatus(int c, String d) {
		this.code = c;
		this.description = d;
	}
	
	public int getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}
}
