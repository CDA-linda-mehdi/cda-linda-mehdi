package cda.http.server;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;

public class ServeurAuxiliaire implements Runnable {
	private final Socket client;

	public ServeurAuxiliaire(Socket c) {
		this.client = c;
	}

	@Override
	public void run() {
		try {
			InputStream clientIn = client.getInputStream();
			OutputStream clientOut = client.getOutputStream();
			BufferedReader br = new BufferedReader(new InputStreamReader(clientIn));

			String uneLigne = br.readLine();
			System.out.println(uneLigne);
			String[] requeteTab = uneLigne.split(" ");
//			File file = new File(
//					"C:\\git-repos\\git-linda\\cda-linda-mehdi\\java-reseau-http2\\src\\ressources\\bonjour.html");
			File file = new File("C:\\outils\\cda-www" + requeteTab[1]); //afin de donner acc�s dossier cda-www
			if (file.getCanonicalPath().contains("cda-www")) {
				if (requeteTab[1].endsWith("html")) {
					clientOut.write(("HTTP/1.1 " + CdaStatus.OK.getCode()
					+ " tout va bien \n" +" " + CdaStatus.OK.getDescription()).getBytes());
			clientOut.write("content-type:text/html; charset=UTF-8\n".getBytes());
			clientOut.write("\n".getBytes());
			br = new BufferedReader(new FileReader(file));
			String line;
			while ((line = br.readLine()) != null) {
				clientOut.write(line.getBytes());
			}
				} else if (requeteTab[1].endsWith("jpg")) {
					clientOut.write(("HTTP/1.1 " + CdaStatus.OK.getCode()
					+ " tout va bien \n" +" " + CdaStatus.OK.getDescription()).getBytes());
			clientOut.write("content-type:image/jpg; charset=UTF-8\n".getBytes());
			clientOut.write("\n".getBytes());
			//br = new BufferedReader(new FileReader(file));
			//String line;
			FileInputStream fis = new FileInputStream(file);
			int nbr = 0;
			//fis.close();
			byte [] tampon = new byte[100];
			
			while ((nbr = fis.read(tampon)) != -1) {
				clientOut.write(tampon, 0, nbr);
			}
				}
					
				
			} else if ("get".equalsIgnoreCase(requeteTab[0])) {
				clientOut.write(("HTTP/1.1 " + CdaStatus.OK.getCode()
						+ " tout va bien \n" + " " + CdaStatus.OK.getDescription()).getBytes());
				clientOut.write("content-type:text/html; charset=UTF-8\n".getBytes());
				clientOut.write("\n".getBytes());
				clientOut.write("Fichier introuvable !".getBytes());
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				this.client.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

}
