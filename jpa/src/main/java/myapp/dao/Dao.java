package myapp.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.NamedQuery;
import javax.persistence.Persistence;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

import myapp.model.Person;

@NamedQuery(name = "findAllPerson", query = "SELECT p* FROM tpERSON p")
@NamedQuery(name = "listPrenom", query = "SELECT new myapp.model.FirstName(p.id,p.firstName)\r\n" + 
		"FROM Person p")
public class Dao {

	private EntityManagerFactory factory = null;

	public void init() {
		factory = Persistence.createEntityManagerFactory("linda-jpa");
	}

	public void close() {
		if (factory != null) {
			factory.close();
		}
	}

//	public Person addPerson(Person p) {
//		EntityManager em = null;
//		try {
//			em = factory.createEntityManager();
//			em.getTransaction().begin();
//			// utilisation de l'EntityManager
//			em.persist(p);
//			em.getTransaction().commit();
//			System.err.println("addPerson witdh id=" + p.getId());
//			return (p);
//		} finally {
//			if (em != null) {
//				em.close();
//			}
//		}
//	}

	
	// Creer un EM et ouvrir une transaction
	private EntityManager newEntityManager() {
		EntityManager em = factory.createEntityManager();
		em.getTransaction().begin();
		return (em);
	}

	// Fermer un EM et defaire la transaction si necessaire
	private void closeEntityManager(EntityManager em) {
		if (em != null) {
			if (em.isOpen()) {
				EntityTransaction t = em.getTransaction();
				if (t.isActive()) {
					try {
						t.rollback();
					} catch (PersistenceException e) {
					}
				}
				em.close();
			}
		}
	}

	// Nouvelle version simplifiee
	public Person addPerson(Person p) {
		EntityManager em = null;
		try {
			em = newEntityManager();
			// utilisation de l'EntityManager
			em.persist(p);
			em.getTransaction().commit();
			System.err.println("addPerson witdh id=" + p.getId());
			return (p);
		} finally {
			closeEntityManager(em);
		}
	}
	
	public Person findPerson(long id) {
		EntityManager em = null;
		try {
			em = factory.createEntityManager();
			em.getTransaction().begin();
			// utilisation de l'EntityManager
			Person p = em.find(Person.class, id);
			p.getCars().size();
			return p;
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}


	public void updatePerson(Person p) {
		EntityManager em = null;
		try {
			em = newEntityManager();
			// utilisation de l'EntityManager
			em.merge(p);
			em.getTransaction().commit();
			System.err.println("mergePerson witdh id=" + p.getId());
		} finally {
			closeEntityManager(em);
		}
	}

	public void removePerson(long id) {
		EntityManager em = null;
		try {
			em = newEntityManager();
			// utilisation de l'EntityManager
			em.remove(id);
			em.getTransaction().commit();
			System.err.println("removePerson witdh id=" + id);
		} finally {
			closeEntityManager(em);
		}
	}

	public List<Person> findAllPersons() {
		EntityManager em = null;
		try {
			em = newEntityManager();
			String query = "SELECT p FROM Person p";
			TypedQuery<Person> q = em.createQuery(query, Person.class);
			return q.getResultList();
		} finally {
			closeEntityManager(em);
		}
	}
	
	public List<Person> findAllPersonsByFirstName() {
		EntityManager em = null;
		try {
			em = newEntityManager();
			String query = "SELECT p.* FROM Person p WHERE first_name like ";
			TypedQuery<Person> q = em.createQuery(query, Person.class);
			return q.getResultList();
		} finally {
			closeEntityManager(em);
		}
	}
	
	
	
}
