package myapp.dao;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import myapp.model.Car;
import myapp.model.Person;

@TestMethodOrder(OrderAnnotation.class)
public class TestDao {

	static Dao dao;

	@BeforeAll
	public static void beforeAll() {
		dao = new Dao();
		dao.init();
	}

	@AfterAll
	public static void afterAll() {
		dao.close();
	}

	private static Person person;
	private static String firstName;
	private static List<Car> cars;
	
	@Order(1)
	@Test
	void addPerson() {
		Person p = new Person();
		firstName = "coucou"+System.currentTimeMillis();
		p.setFirstName(firstName);
		p.setBirthDay(Date.from(LocalDateTime.of(1999, 12, 12, 0, 0).toInstant(ZoneOffset.UTC)));
		
		person = dao.addPerson(p);
		assertNotNull(person);
		assertNotEquals(0, person.getId());
	}
	
	@Order(2)
	@Test
	void findPerson() {
		Person findPerson = dao.findPerson(person.getId());
		System.out.println(findPerson);
		assertNotNull(findPerson);
		assertEquals(findPerson.getFirstName(), person.getFirstName());
	}

	@Order(3)
	@Test
	void updatePerson() {
		firstName = "coucou"+System.currentTimeMillis();
		person.setFirstName(firstName);
		dao.updatePerson(person);
		Person personTmp = dao.findPerson(person.getId());
		assertNotNull(personTmp);
		assertEquals(firstName, person.getFirstName());
	}
	
	@Order(4)
	@Test
	@Disabled
	void removePerson() {
		dao.removePerson(person.getId());
		Person personTmp = dao.findPerson(person.getId());
		assertNull(personTmp);
	}
	
	@Order(5)
	@Test
	void findPerson2() {
		Collection<Person> persons = dao.findAllPersons();
		assertNotEquals(0, persons.size());
		persons.stream().forEach(System.out::println);
	}

	

}
