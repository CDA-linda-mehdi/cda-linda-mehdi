package Exo1.c;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JFrame;

import Exo1.b.MonEcouteur;

public class MaFenetre extends JFrame {

	public MaFenetre(int posX, int posY, String titre) {
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setBounds(posX, posY, 500, 700);
		MouseAdapter adaptateur = new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				System.out.println("appui " +e.getPoint());
			}
		};
		MouseAdapter adaptateur2 = new MouseAdapter() {
			public void mouseReleased(MouseEvent e) {
				System.out.println("relāchement " +e.getPoint());
			}
		};
		this.addMouseListener(adaptateur);
		this.addMouseListener(adaptateur2);
		this.setTitle(titre);
		this.setVisible(true);
	}
}
