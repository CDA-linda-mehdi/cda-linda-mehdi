package Exo1.a;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JFrame;

public class MaFenetre extends JFrame implements MouseListener {

	public MaFenetre(int posX, int posY, String titre) {
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setBounds(posX, posY, 500, 700);
		this.addMouseListener(this);//pour que la fen�tre soit son propre �couteur : on ajoute donc un �couteur et en param�tre on lui indique cette f�n�tre avec "this"
		this.setTitle(titre);
		this.setVisible(true);
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		System.out.println("Clic" +e.getPoint());
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		System.out.println("Enter" +e.getPoint());
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		System.out.println("Exit" +e.getPoint());
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		System.out.println("Press" +e.getPoint());
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		System.out.println("Release" +e.getPoint());
		
	}
}
