package Exo1.b;

import javax.swing.JFrame;

public class MaFenetre extends JFrame {
	
	
	
	public MaFenetre(int posX, int posY, String titre) {
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setBounds(posX, posY, 500, 700);
		MonEcouteur ecouteur = new MonEcouteur();
		this.addMouseListener(ecouteur);
		this.setTitle(titre);
		this.setVisible(true);
	}

}
