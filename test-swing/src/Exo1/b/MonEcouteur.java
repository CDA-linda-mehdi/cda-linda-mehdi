package Exo1.b;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class MonEcouteur implements MouseListener {

	@Override
	public void mouseClicked(MouseEvent e) {
		System.out.println("Clic" +e.getPoint());
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		System.out.println("Enter" +e.getPoint());
	}

	@Override
	public void mouseExited(MouseEvent e) {
		System.out.println("Exit" + e.getPoint());
	}

	@Override
	public void mousePressed(MouseEvent e) {
		System.out.println("Press" +e.getPoint());
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		System.out.println("Release" +e.getPoint());
	}

}
