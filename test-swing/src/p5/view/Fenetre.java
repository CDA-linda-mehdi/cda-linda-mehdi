package p5.view;

import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import p5.controleur.ControleurAjout;
import p5.modele.Compte;

public class Fenetre extends JFrame {

	public Fenetre() {
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setTitle("exemple MVC");
		this.setSize(300,300);
		this.setLocation(200, 200);

		this.setLayout(new BorderLayout());

		JLabel soldeLabel = new JLabel("0");

		JButton bAjout = new JButton("++");
		bAjout.addActionListener(new ControleurAjout(soldeLabel));

		JButton bDebit = new JButton("--");
		bDebit.addActionListener(e->{
			if(Compte.solde > 0) {
				Compte.solde--;
				soldeLabel.setText(Integer.toString(Compte.solde));
			}
		});

		this.getContentPane().add(bAjout,BorderLayout.EAST);
		this.getContentPane().add(bDebit,BorderLayout.WEST);
		this.getContentPane().add(soldeLabel,BorderLayout.CENTER);
		this.getContentPane().add(new JTextField(),BorderLayout.CENTER);

		this.setVisible(true);
	}

}
