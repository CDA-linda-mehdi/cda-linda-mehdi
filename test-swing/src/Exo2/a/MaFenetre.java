package Exo2.a;

import javax.swing.JFrame;

public class MaFenetre extends JFrame {
	
	public MaFenetre (int posX, int posY, String titre) {
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setBounds(posX, posY, 400, 200);
		this.setTitle(titre);
		this.setVisible(true);
	}
}
