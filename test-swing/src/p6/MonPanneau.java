package p6;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

public class MonPanneau extends JPanel {
	public boolean dessin = false;

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		if (dessin) {
			g.setColor(Color.red);
			g.drawLine(150,70,270,170);
			g.setColor(Color.blue);
			g.drawLine(150,170,270,70);
		}
	}
}
