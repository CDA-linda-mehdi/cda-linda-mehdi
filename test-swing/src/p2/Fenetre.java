package p2;

import javax.swing.JFrame;

public class Fenetre extends JFrame {
	
	public Fenetre(int posX, int posY, String titre) {
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setBounds(posX, posY, 400, 300);
		this.setTitle(titre);
		this.setVisible(true);
	}
}