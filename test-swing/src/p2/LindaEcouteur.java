package p2;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

public class LindaEcouteur extends WindowAdapter {
	@Override
	public void windowOpened(WindowEvent arg0) {
		System.out.println("la fen�tre a �t� rendue visible (Opened)");
	}
	
	@Override
	public void windowActivated(WindowEvent arg0) {
		System.out.println("la fen�tre devient la fen�tre active (Activated)");	
	}

	@Override
	public void windowClosed(WindowEvent arg0) {
		System.out.println("la fen�tre a �t� ferm�e (Closed)");
	}

	@Override
	public void windowClosing(WindowEvent arg0) {
		System.out.println("la fen�tre est en cours de fermeture (Closing)");
	}

	@Override
	public void windowDeactivated(WindowEvent arg0) {
		System.out.println("la fen�tre est sur le point de devenir inactive (Deactivated)");
	}

	@Override
	public void windowDeiconified(WindowEvent arg0) {
		System.out.println("la fen�tre a �t� agrandie (De-iconified)");
	}

	@Override
	public void windowIconified(WindowEvent arg0) {
		System.out.println("la fen�tre a �t� r�duite (Iconified)");	
	}

}
