package p4;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.util.stream.Stream;

import javax.swing.JButton;
import javax.swing.JFrame;

public class MaFenÍtre extends JFrame {

	private final JButton bouton1;
	private final JButton bouton2;
	
	public MaFenÍtre() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(10, 20, 400, 300);
		setTitle("FenÍtre principale");
		
		bouton1 = new JButton();
		bouton1.setText("azerazereza");
		bouton1.setSize(30, 30);

		bouton2 = new JButton();
		bouton2.setText("validate");
		bouton2.setSize(200, 20);
		
		Container contentPane = this.getContentPane();
		
		BorderLayout bo = new BorderLayout();
		contentPane.setLayout(bo);
		
		contentPane.add(bouton2,BorderLayout.WEST);
		contentPane.add(bouton1,BorderLayout.SOUTH);

		//Stream.iterate(0, i->i+1).limit(7).forEach(x->contentPane.add(new JButton("B"+x)));
		
//		bouton.addActionListener(e->{System.out.println("bouton1");});
//		bouton1.addActionListener(e->{System.out.println("bouton2");});
		
		ButonEcouteur be = new ButonEcouteur(this);
		bouton2.addActionListener(be);
		bouton1.addActionListener(be);
		this.setVisible(true);
	}
	
	public static void main(String[] args) {
		new MaFenÍtre();
	}

	public JButton getBouton1() {
		return bouton1;
	}

	public JButton getBouton2() {
		return bouton2;
	}
}
