package p4;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

public class ButonEcouteur implements ActionListener {

	private MaFenÍtre laFenetre;
	
	public ButonEcouteur(MaFenÍtre maFenÍtre) {
		this.laFenetre = maFenÍtre;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JButton boutonSource =(JButton)e.getSource();
		if(this.laFenetre.getBouton1().equals(boutonSource)) {
			System.out.println("buton 1");
		} else {
			System.out.println("buton 2");
		}
	}

}
