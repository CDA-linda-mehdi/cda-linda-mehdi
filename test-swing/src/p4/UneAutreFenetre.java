package p4;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class UneAutreFenetre {
	public static void main(String[] args) {
		
		JFrame uneFenetre = new JFrame();
		uneFenetre.setSize(300, 300);
		uneFenetre.setDefaultCloseOperation(3);
		Container tableauGeneral = uneFenetre.getContentPane();
		
		JPanel petitTab1 = new JPanel();
		petitTab1.setBackground(Color.blue);
		
		JPanel petitTab2 = new JPanel();
		petitTab2.setBackground(Color.red);
		
		tableauGeneral.setLayout(new BorderLayout());
		tableauGeneral.add(petitTab1, BorderLayout.WEST);
		tableauGeneral.add(petitTab2, BorderLayout.EAST);
		
		petitTab1.setLayout(new GridLayout(2, 2));
		petitTab1.add(new JButton("b1"));
		petitTab1.add(new JButton("b2"));
		petitTab1.add(new JButton("b3"));
		
		petitTab2.setLayout(new FlowLayout());
		petitTab2.add(new JButton("b1"));
		petitTab2.add(new JButton("b2"));
		petitTab2.add(new JButton("b3"));
		
		uneFenetre.setVisible(true);
	}
}
