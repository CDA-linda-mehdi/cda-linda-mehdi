package p3;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class MonEcouteur extends WindowAdapter {
	public void windowClosing(WindowEvent e) {
		System.out.println("C'est fini...");
		System.exit(0);
	}
	@Override
	public void windowIconified(WindowEvent e) {
			System.out.println("action iconified");
	}
}
