package cda.org.maven_exo2;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ImplBinStringTest {
	
	BinString limplementationATester;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
		limplementationATester = new ImplBinString();
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void test_convert() {
		try {
			assertEquals("11000011", limplementationATester.convert("ab"));
		} catch (ChaineVideException e) {
			fail("Faux");
		}
	}
	
	@Test
	void test_convert_param_chaine_vide() {
		String str="";
		try {
			limplementationATester.convert(str);
			fail("Faux pour une chaîne vide");
		}
		catch (Exception e) {
			assertTrue(e instanceof ChaineVideException);
		}
	}
	
	@Test
	void test_convert_param_chaine_null() {
		String str=null;
		try {
			limplementationATester.convert(str);
			
			fail("Faux pour une chaîne nulle");
		}
		catch (Exception e) {
			assertTrue(e instanceof ChaineVideException);
		}
	}
	
	@Test
	void test_sum() {
		try {
			assertEquals(195, limplementationATester.sum("ab"));
		} catch (Exception e) {
			fail("Exception pour le cas ab "+e.getMessage());
		}
	}
	
	@Test
	void test_sum_param_chaine_vide() {
		String str="";
		try {
			limplementationATester.sum(str);
			fail("Faux pour une chaîne vide");
		}
		catch (Exception e) {
			assertTrue(e instanceof ChaineVideException);
		}
	}
	
	@Test
	void test_sum_param_chaine_null() {
		String str=null;
		try {
			limplementationATester.sum(str);
			fail("Faux pour une chaîne nulle");
		}
		catch (Exception e) {
			assertTrue(e instanceof ChaineVideException);
		}
	}
	
	@Test
	void testBinarise()  {
		try {
			assertEquals("11000011", limplementationATester.binarise(195));
		} catch (Exception e) {
			fail("Faux");
		}
	}
	
	@Test
	void test_Binarise_param_negatif() {
		int a=-1;
		try {
			limplementationATester.binarise(a);
			fail("Faux pour un paramètre négatif");
		}
		catch (Exception e) {
			assertTrue(e instanceof ParametreNegatifException);
		}
	
	}

}
