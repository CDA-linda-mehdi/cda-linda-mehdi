package cda.org.maven_exo2;


public interface BinString {
	
	
	
	
	 // transforme une chaine de caractère en la représentation binaire
	 // de la somme du code ascii des caractères.
	 // si paramètre chaine vide ou paramètre null lève une ChaineVideException
	 public String convert(String s) throws ChaineVideException;
	 
	 
	 
	 // retourne la somme du code ascii des caractères du paramètre
	 // si paramètre chaine vide ou paramètre null lève une ChaineVideException
	 public int sum(String s) throws ChaineVideException;
	 
	 
	 // retourne la représentation binaire du paramètre
	 // sans padding à gauche avec des zéros
	 // si paramètre négatif lève une ParametreNegatifException
	 public String binarise(int x) throws ParametreNegatifException;

}
