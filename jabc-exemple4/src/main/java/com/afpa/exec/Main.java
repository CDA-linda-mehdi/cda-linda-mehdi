package com.afpa.exec;

import com.afpa.dao.Dao;
import com.afpa.dao.PersonneBddDaoImpl;
import com.afpa.modele.Personne;

public class Main {
	public static void main(String args []) {
		Dao<Personne> personneDao = new PersonneBddDaoImpl();
		
		Personne personne = new Personne ("Wick","John");
		Personne insertedPersonne = personneDao.save(personne);
		if (insertedPersonne != null) {
			System.out.println("personne num�ro " + insertedPersonne.
					getNum() + " a �t� ins�r�e");
		}
		else {
			System.out.println("probl�me d�insertion");
		}
		
		personneDao.getAll().stream().forEach(System.out::println);
	}
}