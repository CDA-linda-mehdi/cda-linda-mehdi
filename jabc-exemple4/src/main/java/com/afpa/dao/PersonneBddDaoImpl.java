package com.afpa.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.afpa.conection.MyConnection;
import com.afpa.modele.Personne;

public class PersonneBddDaoImpl implements Dao<Personne> {

	// Supprime une personne
	public void remove(Personne personne) {
		// TODO Auto-generated method stub

	}

	//met � jour les informations d'une personne
	public Personne update(Personne personne) {
		// TODO Auto-generated method stub
		return null;
	}

	//liste les personnes
	public List<Personne> getAll() {
		Connection c = MyConnection.getConnection();
		List<Personne> res = null;
		try {
			PreparedStatement selectAllPs = c.prepareStatement("SELECT * FROM Personne;");
			ResultSet result = selectAllPs.executeQuery();
			res = new ArrayList<>();
			while (result.next()) {
				// on indique chaque fois le nom de la colonne et le type
				int idPersonne = result.getInt("num");

				String nom = result.getString("nom");

				String prenom = result.getString("prenom");

				// pareil pour tous les autres attributs
				Personne p = new Personne(nom, prenom);
				p.setNum(idPersonne);
				res.add(p);
			}
			return res;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return null;
	}

	//ajoute une personne
	public Personne save(Personne personne) {
		Connection c = MyConnection.getConnection();
		if (c != null) {
			try {
				PreparedStatement ps = c.prepareStatement("insert into personne (nom,prenom) values (?,?); ", PreparedStatement.RETURN_GENERATED_KEYS);
				ps.setString(1, personne.getNom());
				ps.setString(2, personne.getPrenom());
				ps.executeUpdate();
				ResultSet resultat = ps.getGeneratedKeys();
				if (resultat.next()) {
					personne.setNum(resultat.getInt(1));
					return personne;
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	//trouve une personne � partir de son ID
	public Personne findById(int id) {
		Connection c = MyConnection.getConnection();
		if (c != null) {
			try {
				PreparedStatement ps = c.prepareStatement("select * from personne where num = ?; ");
				ps.setInt(1, id);
				ResultSet r =ps.executeQuery();
				if (r.next())
					return new Personne(r.getInt("num"),r.getString("nom"),r.getString("prenom"));
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}



}