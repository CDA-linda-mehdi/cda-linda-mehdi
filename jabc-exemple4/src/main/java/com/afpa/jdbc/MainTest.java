package com.afpa.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class MainTest {

	private static Connection connection;
	private static boolean authentifie = false;
	private static PreparedStatement insertionPS;
	private static PreparedStatement selectAllPs;
	private static PreparedStatement loginPs;
	
	private static void initConnection() throws Exception {
		String url = "jdbc:mysql://localhost:3306/jdbc-test?zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=UTC";
		String user = "user1";
		String password = "pwd1";
		connection = DriverManager.getConnection(url, user, password);
		
		connection.setAutoCommit(false);
		
		insertionPS = connection.prepareStatement("INSERT INTO Personne (nom,prenom)	VALUES (?,?);",PreparedStatement.RETURN_GENERATED_KEYS);
		selectAllPs = connection.prepareStatement("SELECT * FROM Personne;");
		loginPs = connection.prepareStatement("select * from personne where nom = ? ;");
	}

	private static void ajouterUnePersonne(Scanner sc) throws Exception {
		if (!authentifie) {
			System.out.println("veuillez d'abord vous authentifier !!! ");
			System.out.println("veuillez vous loguer.");
			return;
		}
		System.out.println("ajout personne : ");
		System.out.print(" saisir nom >");
		String nom = sc.nextLine();
		System.out.print(" saisir prenom >");
		String prenom = sc.nextLine();

		insertionPS.setString(1, nom);
		insertionPS.setString(2, prenom);
		
		int nbr = insertionPS.executeUpdate();
		if (0 != nbr)
			System.out.println("insertion r�eussie");
	}

	private static void afficherPersonnes() throws Exception {
		if (!authentifie) {
			System.out.println("veuillez d'abord vous authentifier !!! ");
			System.out.println("veuillez vous loguer.");
			return;
		}
		ResultSet result = selectAllPs.executeQuery();
		while (result.next()) {
			// on indique chaque fois le nom de la colonne et le type
			int idPersonne = result.getInt("num");

			String nom = result.getString("nom");

			String prenom = result.getString("prenom");

			// pareil pour tous les autres attributs
			System.out.println(idPersonne + " " + nom + " " + prenom);
		}
		System.out.println();
	}

	public static void main(String[] args) throws Exception {
		
		final List<Integer> actionsPrivees = Arrays.asList(0,1,2,4,5);
		final List<Integer> actionsPubliques = Arrays.asList(0,3);
		
		try {
			initConnection();

			Scanner sc = new Scanner(System.in);

			int choix = 0;
			boolean continuer = true;

			while (continuer) {
				System.out.println();
				
				if(authentifie) {
					System.out.println("0- arr�ter le programme");
					System.out.println("1- ajouter une personne");
					System.out.println("2- lister les personnes");
					System.out.println("4- valider modifications");
					System.out.println("5- annuler modifications");
				} else {
					System.out.println("0- arr�ter le programme");
					System.out.println("3- login");
				}
				
				System.out.print("> ");

				choix = sc.nextInt();
				sc.nextLine();

				if( (authentifie && !actionsPrivees.contains(choix))
					|| (!authentifie && !actionsPubliques.contains(choix)) ) {
					choix = -1;
				}
				
				switch (choix) {
				case 0:
					System.out.println("au revoir !");
					continuer = false;
					break;
				case 1:
					ajouterUnePersonne(sc);
					break;
				case 2:
					afficherPersonnes();
					break;
				case 3:
					login(sc);
					break;
				case 4:
					validerModifications();
					break;
				case 5:
					annulerModifications();
					break;
				default:
					System.out.println("action invalide !");
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (connection != null) {
				connection.close();
			}
		}
	}

	private static void annulerModifications() throws Exception {
		connection.rollback();
	}

	private static void validerModifications() throws Exception {
		connection.commit();
	}

	private static void login(Scanner sc) throws Exception {
		System.out.println("entrez votre nom : ");
		System.out.print(">");
		String nom = sc.nextLine();

		loginPs.setString(1, nom);
		
		ResultSet result = loginPs.executeQuery();
		
		System.out.println(loginPs);
		
		if (result.next()) {
			authentifie = true;
			System.out.println("auth reussie !");
		} else {
			System.out.println("mauvais login mot de passe !!");
		}
	}

}
