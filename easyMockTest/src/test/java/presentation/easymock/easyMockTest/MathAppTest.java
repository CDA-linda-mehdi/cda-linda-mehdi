package presentation.easymock.easyMockTest;



import static org.junit.jupiter.api.Assertions.assertEquals;

import org.easymock.EasyMock;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;




public class MathAppTest {
	private MathApplication mathApp;
	private CalculatorService calcServ;
	
	@BeforeEach
	public void setUp() throws Exception {
		mathApp = new MathApplication();
		calcServ = EasyMock.createMock(CalculatorService.class);
		mathApp.setCalcServ(calcServ);
	}
	@AfterEach
	public void tearDown() throws Exception {
	}
	
	@Test
	public void testAddAndSubstract() {
		//Add the behavior to add numbers
		EasyMock.expect(calcServ.add(20.0, 10.0)).andReturn(30.0);
		//Add the behavior to substract numbers
		EasyMock.expect(calcServ.substract(20.0, 10.0)).andReturn(10.0);
		//Activate the mock
		EasyMock.replay(calcServ);
		//Test the substract functionality
		assertEquals(mathApp.substract(20.0, 10.0),10.0,0);
		//Test the add functionality
		assertEquals(mathApp.add(20.0, 10.0),30.0,0);
		//verify call to calcServ is made or not
		EasyMock.verify(calcServ);
	}
	@Test
	public void testMultyply() {
		//Add the behavior to add numbers
		EasyMock.expect(calcServ.multiply(2.0, 10.0)).andReturn(20.0);
		//Activate the mock
		EasyMock.replay(calcServ);
		//Test the multiply functionality
		assertEquals(mathApp.multiply(2.0, 10.0),20.0,0);
		//verify call to calcServ is made or not
		EasyMock.verify(calcServ);
	}
	@Test
	public void testDivide() {
		//Add the behavior to add numbers
		EasyMock.expect(calcServ.divide(20.0, 10.0)).andReturn(2.0);
		//Activate the mock
		EasyMock.replay(calcServ);
		//Test the divide functionality
		assertEquals(mathApp.divide(20.0, 10.0),2.0,0);
		//verify call to calcServ is made or not
		EasyMock.verify(calcServ);
	}
}