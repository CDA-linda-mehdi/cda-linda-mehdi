package presentation.easymock.easyMockTest;

public class MathApplication {

	private CalculatorService calcServ;
	
	   public void setCalcServ(CalculatorService calcServ) {
	       this.calcServ = calcServ;
	   }
	   public double add(double input1, double input2) {
	       return calcServ.add(input1, input2);
	   }
	   public double substract(double input1, double input2) {
	       return calcServ.substract(input1, input2);
	   }
	   public double multiply(double input1, double input2) {
	       return calcServ.multiply(input1, input2);
	   }
	   public double divide(double input1, double input2) {
	       return calcServ.divide(input1, input2);
	   }
}
