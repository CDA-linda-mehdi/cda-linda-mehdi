package com.afpa.point;

import java.util.ArrayList;
import java.util.List;

import com.afpa.commun.GeoException;

public class PointFactory {
	
	private static final List<Point> POINTS = new ArrayList<Point>();
	
	public static Point creer(int a, int c, String n) throws GeoException {
		if(n == null 
				|| n.length()!=2 
				|| !( Character.isAlphabetic(n.charAt(0)) 
						&& Character.isDigit(n.charAt(1)))) {
			throw new NomPointInvalidException();
		}
		
		Point p = new Point(a, c, n);
		
		if(POINTS.contains(p)) {
			throw new PointExisteDejaException();
		} else {
			POINTS.add(p);
		}
		
		return p;
	}
	
	public static int getNb() {
		return POINTS.size();
	}
	
	public static void vider() {
		POINTS.clear();
	}
	
	public static List<Point> getCopie(){
		return new ArrayList<Point>(POINTS);
	}
}
