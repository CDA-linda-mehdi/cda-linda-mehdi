package com.afpa.point;

import org.apache.commons.math3.util.Precision;

import lombok.EqualsAndHashCode;
import lombok.Getter;

@EqualsAndHashCode(of = { "nom" })
public class Point implements Comparable<Point> {

	@Getter
	private int abs;

	@Getter
	private int ord;

	@Getter
	private String nom;

	@Getter
	private double distance;

	Point(int a, int o, String n) {
		this.abs = a;
		this.ord = o;
		this.nom = n;
		this.distance = Precision.round(Math.sqrt(abs * abs + ord * ord), 3);
	}

	@Override
	public int compareTo(Point p) {
		int res = Precision.compareTo(this.distance, p.distance, 0.0001);
		return (res == 0) ? this.nom.compareTo(p.nom) : res;
	}

	@Override
	public String toString() {
		return "Point " + this.nom + " (" + this.abs + " , " + this.ord + ") - distance : " + this.distance;
	}
}
