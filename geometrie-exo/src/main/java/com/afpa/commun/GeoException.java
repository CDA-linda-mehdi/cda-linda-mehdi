package com.afpa.commun;

public class GeoException extends Exception {

	private static final long serialVersionUID = 1L;

	public GeoException() {
	}
	
	public GeoException(Exception e) {
		super(e);
	}

}
