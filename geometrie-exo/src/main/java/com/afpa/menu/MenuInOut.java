package com.afpa.menu;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class MenuInOut {
	private final OutputStream outMenu;
	private final OutputStream outStatus;
	private final BufferedReader br;

	/**
	 * 
	 * @param in   flux d entrée de la saisie client
	 * @param out  flux de sortie pour afficher les commandes/le menu
	 * @param outS flux de sortie pour afficher le status/message de retour
	 * @throws MenuException
	 */
	public MenuInOut(InputStream in, OutputStream out, OutputStream outS) throws MenuException {
		log.trace("creation instance MenuInOut");
		try {
			this.br = new BufferedReader(new InputStreamReader(in, "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			throw new MenuException(e);
		}
		this.outMenu = out;
		this.outStatus = outS;
	}

	public void ecrire(boolean messageResultat, String msg) throws MenuException {
		this.ecrire(messageResultat, msg, true);
	}
	
	public void ecrire(String msg) throws MenuException {
		this.ecrire(false, msg, true);
	}

	public void ecrire(String msg,boolean avecSautDeLigne) throws MenuException {
		this.ecrire(false, msg, avecSautDeLigne);
	}
	
	public void ecrire(boolean messageResultat, String msg, boolean avecSautDeLigne) throws MenuException {
		try {
			OutputStream out = messageResultat?this.outStatus:this.outMenu;
			log.trace(msg);
			out.write(msg.getBytes());
			if (avecSautDeLigne) {
				out.write('\n');
			}
			out.flush();
		} catch (Exception e) {
			throw new MenuException(e);
		}
	}

	public String lireString() throws MenuException {
		try {
			String res = this.br.readLine();
			log.trace(res);
			return res;
		} catch (Exception e) {
			log.warn(e.getMessage());
			throw new MenuException(e);
		}
	}

	public int lireEntier() throws MenuException {
		return this.lireEntier(false, null, false);
	}

	public int lireEntier(boolean recursif, String messageRecursif, boolean sautDeLigneMessage) throws MenuException {
		try {
			String strTmp = this.br.readLine();
			log.trace(strTmp);
			return Integer.parseInt(strTmp.trim());
		} catch (Exception e) {
			log.warn(e.getMessage());
			if (recursif) {
				this.ecrire(false, messageRecursif, sautDeLigneMessage);
				return this.lireEntier(recursif, messageRecursif, sautDeLigneMessage);
			} else {
				throw new MenuException(e);
			}
		}
	}
}
