package Exos3;

import java.util.ArrayList;
import java.util.List;

public class Tableau extends Thread {
	
	private List<String> liste;
	
	public Tableau(List<String> list) {
		if (list.size() >= 5) {
			this.liste = list;
		} else {
			System.out.println("Le tableau n'est pas assez long");
		}	
	}
	
	public void run() {
		int nb = 0;
		for (String string : liste) {
			if (string.equalsIgnoreCase("toto")) {
				nb++;
			}
		}
		System.out.println(nb);	
	}
	
	
	public static void main(String [] args) {
		
		List<String> liste = new ArrayList<>();
		
		liste.add("toto");
		liste.add("titi");
		liste.add("tata");
		liste.add("toto");
		liste.add("toto");
		liste.add("toto");
		liste.add("titi");
		liste.add("tata");
		liste.add("toto");
		liste.add("toto");
		
		Tableau tab = new Tableau(liste);
		tab.start();
		
	}	
}
