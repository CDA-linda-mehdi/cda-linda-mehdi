package threadsCorrectionRedouane;

import java.util.Random;
public class Compteur extends Thread {
    private String name;
    private int n;
    private static int rang = 0;
    private static final String a = "";
    public Compteur(String name, int n) {
        super();
        this.name = name;
        this.n = n;
    }
    public void run(){
        for (int i = 0; i <= n; i++) {
            System.out.println(name +" : "+ i);
            Random a = new Random();
            long b = a.nextInt(500);
            try {
                Thread.sleep(b);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        synchronized (a) {
            rang++;
            System.out.println(name + " a fini de compter jusqu'a " + n + " en position " + rang);
        }
    }
    public static void main(String[] args) {
        Compteur test = new Compteur("red", 10);
        Compteur test1 = new Compteur("blue", 10);
        Compteur test2 = new Compteur("green", 10);
        Compteur test3 = new Compteur("black", 10);
        test.start();
        test1.start();
        test2.start();
        test3.start();  
    }
}
