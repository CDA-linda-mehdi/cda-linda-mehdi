package Exos2;

public class Operation extends Thread {
	  private Compte compte;

	  public Operation(String nom, Compte compte) {
	    super(nom);
	    this.compte = compte;
	  }

	   public void run() {
	    while (true) {
	      int i = (int) (Math.random() * 10000);//Retourne un nbre al�atoire entre 0.0 et 10000
	      String nom = getName();//r�cup�re le nom du compte
	      System.out.print(nom);
//	        compte.ajouter(i);
//	        compte.retirer(i);
	      compte.operationNulle(i);
	      int solde = compte.getSolde();
	      System.out.print(nom);
	      if (solde != 0) {
		System.out.println(nom + ":**solde=" + solde);
		System.exit(1);
	      }
	    }
	  }

	  public static void main(String[] args) {
	    Compte compte = new Compte();
	    for (int i = 0; i < 20; i++) {//on a 20 Threads qui vont travailer sur le compte
	    	//operation cr�� un objet avec un compte mais plusieur op�rations
	      Operation operation = new Operation("" + (char)('A' + i), compte);//'A' + i renvoie le prochain caract�re : A = 65 en ASCII, donc A + 1 = 66 donc B.
	      operation.start();
	    }
	  }
	}