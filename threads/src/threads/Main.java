package threads;

public class Main {

	public static void main(String[] args) {

		Compteur c1 = new Compteur("toto");
		Compteur c2 = new Compteur("titi");
		Compteur c3 = new Compteur("tata");
		
		Thread t1 = new Thread(new Compteur("toto"));
		Thread t2 = new Thread(new Compteur("titi"));
		Thread t3 = new Thread(new Compteur("tata"));
		
		t1.setPriority(Thread.MAX_PRIORITY);
		t1.start();
		t2.start();
		//t3.start();
	}
}
