package com.afpa.commun;

public class CdaException extends Exception {

	private static final long serialVersionUID = 1L;

	public CdaException() {
	}
	
	public CdaException(Exception e) {
		super(e);
	}

}
