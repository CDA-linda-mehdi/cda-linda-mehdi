package com.afpa.service;

import java.util.List;

import com.afpa.dao.MarqueDao;
import com.afpa.dto.ActionRetourDto;
import com.afpa.entity.Marque;

public class MarqueService {

	private final MarqueDao marqueDao;
	
	public MarqueService() {
		this.marqueDao = new MarqueDao();
	}
	
	public ActionRetourDto creerMarque(String marque) {
		List<Marque> marques = marqueDao.getMarqueByLabel(marque);
		if(marques != null && !marques.isEmpty()) {
			return ActionRetourDto.builder().code(-1).msg("erreur : cette marque existe d�j�").build();
		}
		Marque m = marqueDao.add(Marque.builder().label(marque).build());
		return ActionRetourDto.builder().code(1).msg("creation de la marque code "+m.getCode()+" ok!").build();
	}

}
