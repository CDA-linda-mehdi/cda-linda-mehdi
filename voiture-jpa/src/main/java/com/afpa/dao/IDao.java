package com.afpa.dao;

import java.util.List;

interface IDao<T> {
	public void remove(Object pk);
	public T update(T entity);
	public T add(T entity);
	public List<T> findAllByNamedQuery(String query);
	public T find(Object id);
}
