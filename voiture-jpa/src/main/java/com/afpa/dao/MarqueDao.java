package com.afpa.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import com.afpa.entity.Marque;

public class MarqueDao extends AbstractDao<Marque>{

	public List<Marque> getMarqueByLabel(String marque) {
		EntityManager em = null;
		try {
			em = newEntityManager();
			TypedQuery<Marque> q = em.createNamedQuery("getMarqueByLabel", Marque.class);
			q.setParameter("labelParam", marque);
			return q.getResultList();
		} finally {
			closeEntityManager(em);
		}
	}

}
