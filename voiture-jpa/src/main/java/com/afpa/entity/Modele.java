package com.afpa.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="t_modele")
public class Modele {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int code;
	private String label;
	private int puissance;
	
	@ManyToOne
	@JoinColumn(name = "energie")
	private Energie energie;
	
	@ManyToOne
	@JoinColumn(name = "marque")
	private Marque marque;
}
