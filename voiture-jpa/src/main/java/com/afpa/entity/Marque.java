package com.afpa.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@NamedQueries({
	@NamedQuery(name = "getMarqueByLabel",query = "select m from Marque m where m.label= :labelParam")
})
@Table(name="t_marque")
public class Marque { 
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int code;
	private String label;
}
