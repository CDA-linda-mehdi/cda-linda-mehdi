package com.afpa.ihm;

public class MenuException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public MenuException(Exception e) {
		super(e);
	}

}
