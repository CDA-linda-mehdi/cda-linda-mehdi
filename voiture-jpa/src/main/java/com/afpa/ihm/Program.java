package com.afpa.ihm;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Map;
import java.util.TreeSet;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.afpa.dto.ActionRetourDto;
import com.afpa.service.MarqueService;

public class Program {
	public static void main(String[] args) throws Exception {
		exec(System.in, System.out, System.out);
	}

	public static void exec(InputStream in, OutputStream outMenu, OutputStream outStatus) throws Exception {
		final MenuInOut menu = new MenuInOut(in, outMenu, outStatus);

		menu.ecrire("Bienvenu dans l'appli gestion de voitures ! ");
		
		final MenuArretSignal programEtat = new MenuArretSignal();
		final MarqueService marqueService = new MarqueService();
		
		final TreeSet<MenuAction> actionsSet = new TreeSet<>();
		actionsSet.addAll(Arrays.asList(
				new MenuAction(0, "arreter", ()->programEtat.stop()),
				new MenuAction(1, "ajouter une marque", ()->ajoutMarque(menu,marqueService)),
				new MenuAction(2, "ajouter un modele", ()->System.out.println("pas encore impelementée"))
		));
		
		Map<Integer, MenuAction> actionsMap = actionsSet.stream().collect(Collectors.toMap(MenuAction::getId,Function.identity()));
		
		final MenuAction choixIncorrectAction = new MenuAction(-1,"",()->menu.ecrire("choix incorrect"));
		
		int choix = 0;

		while (programEtat.isContinuer()) {
			menu.ecrire("choisir une action");
			actionsSet.forEach(a->menu.ecrire(a.getId()+"-"+a.getDesc()));
			
			menu.ecrire(" > ", false);
			choix = menu.lireEntier(true, "saisir une valeur numérique entre 1 et 7\n > ", false);
			
			actionsMap.getOrDefault(choix, choixIncorrectAction).run();
			
			menu.ecrire("");
		}
	}

	private static void ajoutMarque(MenuInOut menu, MarqueService marqueService) {
		menu.ecrire("saisir le nom de la marque :");

		menu.ecrire(" > ", false);
		String marque = menu.lireString();

		ActionRetourDto res = marqueService.creerMarque(marque);
		menu.ecrire(res.getMsg());
	}
}
