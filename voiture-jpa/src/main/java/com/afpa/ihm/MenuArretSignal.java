package com.afpa.ihm;

import lombok.Getter;

class MenuArretSignal {
	@Getter
	private boolean continuer = true;
	
	public void stop() {
		this.continuer = false;
	}
}
