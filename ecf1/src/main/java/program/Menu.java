package program;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

import com.afpa.ecf1.Point;
import com.afpa.ecf1.PointNomDejaExistantException;

public class Menu {

	@SuppressWarnings("unused")
	public static void main(String[] args) throws PointNomDejaExistantException {
		ArrayList<Point> l = new ArrayList<Point>();
		//List<Point> listPoint = new ArrayList<Point>();
		Point point = null;
		
		Scanner sc = new Scanner(System.in);
		
		boolean b = true;
		String str;
		int rep;
		int rep1;
		int rep2;
		int rep3;
		
		while (b) {
			System.out.println("Veuillez choisir une des options ci-dessous : ");
			System.out.println("1 - Créer des point : ");
			System.out.println("2 - Créer des triangles : ");
			System.out.println("3 - Créer des cercles : ");
			System.out.println("4 - Lister les points par ordre croissant : ");
			System.out.println("5 - Créer les triangles par ordre croissant des surfaces : ");
			System.out.println("6 - Créer les cercles par ordre croissant des surfaces : ");
			System.out.println("7 - Sortir du programme");
			rep = sc.nextInt();
			sc.nextLine();
			
			switch (rep) {
			case 1 :
				
				System.out.println("Indiquer le nom du point : ");
				str = sc.nextLine();
				System.out.println("Indiquer l'abscisse du point : ");
				rep1 = sc.nextInt();
				System.out.println("Indiquer l'ordonnee du point : ");
				rep2 = sc.nextInt();
				point = new Point(str,rep1,rep2);
				System.out.println(point);
				l.add(point);
				break;
			case 2 :
				System.out.println("Indiquer le nom du triangle : ");
				str = sc.nextLine();
				System.out.println("Indiquer le point A : ");
				rep1 = sc.nextInt();
				System.out.println("Indiquer le point B : ");
				rep2 = sc.nextInt();
				System.out.println("Indiquer le point B : ");
				rep3 = sc.nextInt();
				break;
			case 3 :
				
				break;
			case 4 :
				l = (ArrayList<Point>) Point.getListe();
				System.out.println(l);
				break;
			case 5 :
				System.out.println(l);
				break;
			case 6 :
				
				break;
			case 7 :
				System.out.println("Sortie du programme");
				b = false;
				break;
				default :
					System.out.println("Vous n'avez pas saisie un choix de la liste !\n");
				
			}
		}
	}

}
