package com.afpa.ecf1;


import java.util.ArrayList;
import java.util.List;

public class Point {
	private final String nom;
	private final int abs;
	private final int ord;
	private static List<Point> liste = new ArrayList<Point>();

	public Point(String name, int a, int b)  throws PointNomDejaExistantException  {
		this.nom = name;
		this.abs = a;
		this.ord = b;
		boolean avecMemeNomTrouve = false;
		for (Point point : liste) {
			if (point.getNom().equalsIgnoreCase(name)) {
				 throw new PointNomDejaExistantException();
			} 	
		}
		if (! avecMemeNomTrouve) {
			liste.add(this);
		}
	}
	

	public String getNom() {
		return nom;
	}

	public int getAbs() {
		return abs;
	}


	public int getOrd() {
		return ord;
	}

	public static List<Point> getListe() {
		return liste;
	}

	public static void setListe(List<Point> liste) {
		Point.liste = liste;
	}

	@Override
	public String toString() {
		return "Point [nom=" + nom + ", abs=" + abs + ", ord=" + ord + "]\n";
	}

	public void afficheListe() {
		System.out.println(liste);
	}




}
