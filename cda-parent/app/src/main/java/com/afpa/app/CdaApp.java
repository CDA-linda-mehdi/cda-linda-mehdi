package com.afpa.app;

import org.apache.commons.lang3.StringUtils;

import com.afpa.tools.CdaTools;
import com.afpa.tools.ParametreNonValideException;

public class CdaApp {

	 public String doubleFactAndLeftPad(int n) throws ParametreNonValideException {
		 CdaTools cdatools = new CdaTools();
		 return StringUtils.leftPad(""+cdatools.doubleFactoriel(n), 19, '0');
	 }
}
