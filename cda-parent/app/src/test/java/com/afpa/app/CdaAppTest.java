package com.afpa.app;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.afpa.tools.ParametreNonValideException;

class CdaAppTest {
	//	CdaApp cdaapp;
	//
	//	@BeforeAll
	//	static void setUpBeforeClass() throws Exception {
	//	}
	//
	//	@AfterAll
	//	static void tearDownAfterClass() throws Exception {
	//	}
	//
	//	@BeforeEach
	//	void setUp() throws Exception {
	//		cdaapp = new CdaApp();
	//	}
	//
	//	@AfterEach
	//	void tearDown() throws Exception {
	//	}
	//
	//	@Test
	//	void testDoubleFactAndLeftPad() {
	//		try {
	//			cdaapp.doubleFactAndLeftPad(-7);
	//		} catch (ParametreNonValideException e) {
	//			System.out.println("faux");
	//		}
	//	}

	@Test
	void testAppNegatif() {
		CdaApp cda = new CdaApp();
		try {
			cda.doubleFactAndLeftPad(-5);
		} catch (ParametreNonValideException p) {
			System.out.println("parametre négatif");
		}
	}
	@Test
	void testAppSupp() {
		CdaApp cda = new CdaApp();
		try {
			cda.doubleFactAndLeftPad(22);
		} catch (ParametreNonValideException p) {
			System.out.println("parametre supérieur à 19");
		}
	}
	@Test
	void testApp() {
		CdaApp cda = new CdaApp();
		try {
			cda.doubleFactAndLeftPad(10);
		} catch (ParametreNonValideException p) {
		}
	}

}
