package com.afpa.tools;

import org.apache.commons.math3.util.CombinatoricsUtils;

public class CdaTools {

	public long doubleFactoriel(int a) throws ParametreNonValideException {
		if (a < 0 || a >= 19) {
			throw new ParametreNonValideException();
		} else {
			return CombinatoricsUtils.factorial(a) * 2;
		}
	}
}
