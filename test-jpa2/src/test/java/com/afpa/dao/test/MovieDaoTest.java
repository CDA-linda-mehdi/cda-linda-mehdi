package com.afpa.dao.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import com.afpa.dao.MovieDao;
import com.afpa.entity.Movie;

@TestMethodOrder(value = OrderAnnotation.class)
public class MovieDaoTest {
	
	static MovieDao dao;

	@BeforeAll
	public static void beforeAll() {
		dao = new MovieDao();
	}

	@AfterAll
	public static void afterAll() {
		dao.close();
	}

	static Movie movie;
	
	@Test
	@Order(1)
	public void creerFilm() {
		Movie m = Movie.builder().name("M"+System.currentTimeMillis()).build();
		m = dao.add(m);
		assertNotNull(m);
		assertNotEquals(0, m.getId());
		movie = m;
	}
	
	@Test
	@Order(2)
	public void chercherFilm() {
		Movie m = dao.find(movie.getId());
		assertNotNull(m);
		assertEquals(movie.getId(), m.getId());
	}
}
