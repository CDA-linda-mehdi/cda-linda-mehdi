package com.afpa.dao.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Random;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import com.afpa.dao.PersonneDao;
import com.afpa.entity.Car;
import com.afpa.entity.Movie;
import com.afpa.entity.Person;

@TestMethodOrder(value = OrderAnnotation.class)
public class PersonDaoTest {
	
	static PersonneDao dao;

	@BeforeAll
	public static void beforeAll() {
		dao = new PersonneDao();
	}

	@AfterAll
	public static void afterAll() {
		dao.close();
	}

	private static Person person;
	private static String firstName;
	
	@Test
	@Order(1)
	public void ajoutPersonne() {
		Person p = new Person();
		firstName = "coucou"+System.currentTimeMillis();
		p.setFirstName(firstName);
		p.setLastName("kiki");
		p.setBirthDay(Date.from(LocalDateTime.of(1999, 12, 12, 0, 0).toInstant(ZoneOffset.UTC)));
		
		person = dao.add(p);
		assertNotNull(person);
		assertNotEquals(0, person.getId());
	}
	
	@Test
	@Order(2)
	public void chercherPersonne() {
		Person findPerson = dao.find(person.getId());
		System.out.println(findPerson);
		assertNotNull(findPerson);
		assertEquals(findPerson.getFirstName(), person.getFirstName());
	}
	
	@Test
	@Order(3)
	public void miseAjourPersonne() {
		firstName = "coucou"+System.currentTimeMillis();
		person.setFirstName(firstName);
		dao.update(person);
		Person personTmp = dao.find(person.getId());
		assertNotNull(personTmp);
		assertEquals(firstName, person.getFirstName());
	}
	
	@Test
	@Order(4)
	@Disabled
	public void supprimerPersonne() {
		dao.remove(person.getId());
		Person personTmp = dao.find(person.getId());
		assertNull(personTmp);
	}
	
	@Test
	@Order(5)
	public void chercherToutesLesPersonnes() {
		Collection<Person> persons = dao.findAll();
		assertNotEquals(0, persons.size());
		persons.stream().forEach(System.out::println);
	}
	
	@Test
	@Order(6)
	public void ajoutPersonneAvecVoiture() {
		Person p = new Person();
		firstName = "Loulou"+System.currentTimeMillis();
		p.setFirstName(firstName);
		p.setLastName("kiki");
		Random r = new Random();
		p.setBirthDay(Date.from(LocalDateTime.of(1990+r.nextInt(10), r.nextInt(13), r.nextInt(27), 0, 0).toInstant(ZoneOffset.UTC)));
		
		Car c = new Car();
		c.setImmatriculation("IM"+System.currentTimeMillis());
		c.setModel("mod1");
		c.setOwner(p);
		
		Car c2 = new Car();
		c2.setImmatriculation("IM"+System.currentTimeMillis()+1);
		c2.setModel("mod2");
		c2.setOwner(p);

		List<Car> cars = Arrays.asList(c,c2);
		p.setCars(new HashSet<Car>(cars));
		
		person = dao.add(p);
		assertNotNull(person);
		assertNotEquals(0, person.getId());
	}
	
	
	@Test
	@Order(7)
	public void ajoutPersonneAvecMovie() {
		Person p = new Person();
		firstName = "Loulou"+System.currentTimeMillis();
		p.setFirstName(firstName);
		p.setLastName("kiki");
		p.setBirthDay(Date.from(LocalDateTime.of(1999, 12, 12, 0, 0).toInstant(ZoneOffset.UTC)));
		
		Movie m = new Movie();
		m.setName("Le teritoire des loups"+System.currentTimeMillis());
		
		Movie m2 = new Movie();
		m2.setName("Le teritoire des loups"+System.currentTimeMillis()+1);
		
		List<Movie> movies = Arrays.asList(m,m2);
		p.setMovies(new HashSet<Movie>(movies));
		
		person = dao.add(p);
		assertNotNull(person);
		assertNotEquals(0, person.getId());
	}
	
	@Test
	@Order(8)
	void rechereParNom() {
		Person findPerson = dao.findByName(person.getFirstName());
		assertNotNull(findPerson);
		assertEquals(findPerson.getFirstName(), person.getFirstName());
	}
	
}
