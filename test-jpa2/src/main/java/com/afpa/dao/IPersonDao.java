package com.afpa.dao;

import java.util.Collection;

import com.afpa.entity.Person;

public interface IPersonDao extends IDao<Person> {
	public Person findByName(String n);
	public Collection<Person> findAll();
}
