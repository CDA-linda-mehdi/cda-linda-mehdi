package com.afpa.dao;

import java.util.Collection;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import com.afpa.entity.Person;

public class PersonneDao extends AbstractDao<Person> implements IPersonDao {

	@Override
	public Person findByName(String name) {
		EntityManager em = null;
		try {
			em = newEntityManager();
			TypedQuery<Person> res = em.createNamedQuery("findByName", Person.class);
			res.setParameter("fNameParam", name);
			return res.getSingleResult();
		} finally {
			closeEntityManager(em);
		}
	}

	@Override
	public Collection<Person> findAll() {
		return this.findAllByNamedQuery("findAll");
	}

}
