package com.afpa.entity;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@ToString(of = { "id", "firstName", "lastName", "birthDay" })
@Entity()
@Table(name = "t_person", uniqueConstraints = { @UniqueConstraint(columnNames = { "first_name", "birth_day" }) })
@NamedQueries({ @NamedQuery(name = "findAll", query = "SELECT p FROM Person p"),
		@NamedQuery(name = "findByName", query = "SELECT p FROM Person p where p.firstName= :fNameParam"),
		@NamedQuery(name = "findAllNames", query = "SELECT p.firstName FROM Person p"), })
public class Person {

	@Id()
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column(name = "first_name", length = 200, nullable = false)
	private String firstName;

	@Column(name = "last_name", length = 200, nullable = false, unique = false)
	private String lastName;

	@Temporal(TemporalType.DATE)
	@Column(name = "birth_day")
	private Date birthDay;

	@OneToMany(cascade = { CascadeType.MERGE, CascadeType.PERSIST }, fetch = FetchType.LAZY, mappedBy = "owner")
	private Set<Car> cars;

	@ManyToMany(fetch = FetchType.LAZY, cascade = { CascadeType.MERGE, CascadeType.PERSIST })
	@JoinTable(name = "t_Person_Movie", joinColumns = { @JoinColumn(name = "id_person") }, inverseJoinColumns = {
			@JoinColumn(name = "id_movie") })
	private Set<Movie> movies;
}
