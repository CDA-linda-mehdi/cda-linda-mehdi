package com.afpa.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "t_car")
public class Car {
	@Id()
	private String immatriculation;

	@Column(length = 150, nullable = false)
	private String model;

	@ManyToOne(optional = true)
	@JoinColumn(name = "owner")
	private Person owner;
}