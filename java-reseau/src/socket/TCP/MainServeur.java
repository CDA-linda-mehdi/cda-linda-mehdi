package socket.TCP;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketAddress;

public class MainServeur {

	public static void main(String[] args) throws IOException {
		int i = 0;
		System.out.println(i++);
		ServerSocket s = new ServerSocket(8081);
		System.out.println(i++);
		Socket cli = s.accept();
		System.out.println(i++);
		OutputStream os = cli.getOutputStream();
		System.out.println(i++);
		os.write(2);
		System.out.println(i++);
		InputStream is = cli.getInputStream();
		System.out.println(i++);
		int y = is.read();
		System.out.println(i++);
		System.out.println("y = " + y);
		System.out.println(i++);
		cli.close();
		System.out.println(i++);
		s.close();
		System.out.println(i++);
	}

}
