package socket.TCP;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketAddress;

public class MainClient {

	public static void main(String[] args) throws IOException {
		int i = 0;
		System.out.println("j : " + i++);
		Socket cli = new Socket();
		System.out.println("j : " + i++);
		SocketAddress host = new InetSocketAddress("localhost", 8081);
		System.out.println("j : " + i++);
		cli.connect(host);
		System.out.println("j : " + i++);
		InputStream is = cli.getInputStream();
		System.out.println("j : " + i++);
		int x = is.read();
		System.out.println("j : " + i++);
		System.out.println("x = " + x);
		System.out.println("j : " + i++);
		OutputStream os = cli.getOutputStream();
		System.out.println("j : " + i++);
		os.write(x + 1);
		System.out.println("j : " + i++);
		System.out.println("x = " + x);
		System.out.println("j : " + i++);
		cli.close();
		System.out.println("j : " + i++);
	}

}
