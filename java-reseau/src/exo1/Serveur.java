package exo1;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class Serveur {
    public static void main(String[] args) throws IOException {
        int i =0;
        System.out.println("premier passage");
        ServerSocket s = new ServerSocket(8080);
        System.out.println("second passage");
        Socket c1 = s.accept();
        System.out.println("troisieme passage");
        OutputStream os = c1.getOutputStream();
        System.out.println("quatrieme passage");
        // os.write(2);  // premiere ecriture  
        System.out.println("cinquieme passage");
        InputStream is = c1.getInputStream();
        System.out.println("sixieme passage");
        int y = is.read(); // premiere lecture 
        System.out.println("septieme passage");
        System.out.println("y="+y);
        System.out.println("huitieme passage");
        os.write("server".getBytes()); // seconde ecriture
        System.out.println("huitieme et un passage");
        byte[] reqserver = new byte[20];
        y = is.read(reqserver);
        System.out.println("req=<"+new String(reqserver).toUpperCase()+">");
        byte[] reqserver2 = new byte[20];
        y = is.read(reqserver2);
        System.out.println("req=<"+new String(reqserver2).toUpperCase()+">");
        c1.close();
        System.out.println("neuvieme passage");
        System.out.println("dixieme passage");
        
        Socket c2 = s.accept();
        OutputStream os2 = c2.getOutputStream();
        InputStream is2 = c2.getInputStream();
        os2.write("testsocket2".getBytes());
        c2.close();
        s.close();
    }
}




