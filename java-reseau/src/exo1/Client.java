package exo1;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;

public class Client {
    public static void main(String[] args) throws IOException {
        Socket c = new Socket();
        SocketAddress host = new InetSocketAddress("localhost", 8080);
        c.connect(host);
        InputStream is = c.getInputStream();
           //  int x = is.read(); // premiere lecture
        //System.out.println("x1="+x);
        OutputStream os = c.getOutputStream();
        os.write(" je suis client".getBytes());  // premiere ecriture
        byte[] reqclient = new byte[6];
        int x = is.read(reqclient); // seconde lecture
        System.out.println("x2="+x);
        System.out.println("req=<"+new String(reqclient)+">");
        os.write(" je suis client2".getBytes());  // second ecriture
        c.close();
        
        
        
        
        Socket c1 = new Socket();
        SocketAddress host1 = new InetSocketAddress("localhost", 8080);
        c1.connect(host1);
        InputStream is2 = c1.getInputStream();
        byte[] reqclient3 = new byte[6];
        int y = is2.read(reqclient3); // premiere  lecture socket 2
        String s = new String(reqclient3);
        String reverse =tools.reverse(s);
        System.out.println("req=<"+ reverse+">");
        c1.close();
    }
}