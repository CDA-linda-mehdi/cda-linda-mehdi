package com.afpa.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class MainTest {

	public static void main(String[] args) throws Exception {
//		try {
//			Class.forName("com.mysql.cj.jdbc.Driver");
//		} catch (ClassNotFoundException e) {
//			System.out.println(e.getMessage());
//		}
		
		// Charger le pilote. Mais plus obligatoire depuis la serni�re version des drivers
		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e) {
			System.out.println(e.getMessage());
		}

		String url = "jdbc:postgresql://localhost:5432/jdbc-test";
		String user = "postgres";
		String password = "Linda59*";
		Connection connexion = null;
		try {
			connexion = DriverManager.getConnection(url, user, password);

			// cr�eation de la requ�ete (statement)
			Statement statement = connexion.createStatement();

			// Ex�ecution de la requ�ete
			ResultSet result = statement.executeQuery("SELECT * FROM Personne;");
			
			int nbPersonne = 0;
			while (result.next()) {
				nbPersonne++;
				// on indique chaque fois le nom de la colonne et le type
				int idPersonne = result.getInt("num");

				String nom = result.getString("nom");

				String prenom = result.getString("prenom");

				// pareil pour tous les autres attributs
				System.out.println(idPersonne + " " + nom + " " + prenom);
			}
			
			System.out.println("le nombre de personne : "+nbPersonne);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (connexion != null)
				try {
					connexion.close();
				} catch (SQLException ignore) {
					ignore.printStackTrace();
				}
		}

	}

}
