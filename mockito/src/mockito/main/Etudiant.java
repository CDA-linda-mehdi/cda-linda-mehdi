package mockito.main;

public class Etudiant {

	private String name;
	public BloodType aE;
	
	public String getName () {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String infoOnStudent(Etudiant e) { //Dans la m�thode info on applelle la m�thode getGroupeSanguain de l'interface
		String retour = e.getName() + " "+ e.aE.getGroupeSanguain(e).toString();
        System.err.println(retour);
        return retour;
	}
}
