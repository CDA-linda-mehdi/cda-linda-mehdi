package mockito.test;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import mockito.main.BloodType;
import mockito.main.Etudiant;

class EtudiantTest {
	Etudiant e;

	@BeforeEach
	void setUp() throws Exception {
		System.err.println("etape1");
		e = new Etudiant();
		e.setName("Clement");
		System.err.println("J'ai nomm�");
		e.aE = mock(BloodType.class);
	}
 
	@Test
	void test() {
		System.err.println("je lance le test");
		System.err.println("je vais passer par when");
		when(e.aE.getGroupeSanguain(e)).thenReturn("AB+");
		System.err.println("je suis pass� par when");
		assertTrue(e.infoOnStudent(e).equals("Clement AB+"));
	}
}
