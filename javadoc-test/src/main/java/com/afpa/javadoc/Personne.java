package com.afpa.javadoc;

/**
 * La classe personne modelise une personne . Elle sert pour . . .
 *
 * @see NegativeAgeException
 * @author fab
 */
public class Personne {
	/**
	 * l � age de l a personne , d o i t et re p o s i t i f
	 */
	private int age;
	/**
	 * l e nom de l a personne , une simple chaine de carateres
	 */
	private String name;

	/**
	 * Const ructeur pour l a classe Personne . Blabla . . .
	 *
	 * @param age  un e n t i e r >= 0
	 * @param name l e nom de l a personne
	 * @throws NegativeAgeException s i age < 0
	 */
	public Personne(int age, String name) throws NegativeAgeException {
		this.setAge(age);
		this.name = name;
	}

	/**
	 * Modi f ie l � age de l a personne .
	 *
	 * @param age un e n t i e r >= 0
	 * @throws NegativeAgeException s i age < 0
	 */
	public void setAge(int age) throws NegativeAgeException {
		if (age < 0) {
			throw new NegativeAgeException(" er reur : l � age d o i t et re > 0 , " + " valeur ent ree : " + age); // s
		}
		this.age = age;
	}

	@Override
	/**
	 * cree une representat ion sous forme de s t r i n g de l � ob j e t personne
	 * 
	 * @return une chaine de caracteres
	 */
	public String toString() {
		return name + " age : " + age;
	}

}
