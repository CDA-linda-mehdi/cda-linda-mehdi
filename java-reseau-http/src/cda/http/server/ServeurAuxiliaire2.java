package cda.http.server;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;

public class ServeurAuxiliaire2 implements Runnable {
	private final Socket client;

	public ServeurAuxiliaire2(Socket c) {
		this.client = c;
	}

	@Override
	public void run() {
		try {
			InputStream clientIn = client.getInputStream();
			OutputStream clientOut = client.getOutputStream();
			BufferedReader br = new BufferedReader(new InputStreamReader(clientIn));

			String uneLigne = br.readLine();
			System.out.println(uneLigne);
			String[] requeteTab = uneLigne.split(" ");
			File file = new File("C:\\outils\\cda-www\\bonjour.html");
			if (file.isFile()) {
				System.out.println(uneLigne);
				if ("get".equalsIgnoreCase(requeteTab[0])) {
					clientOut.write(
							("HTTP/1.1 "+ CdaStatus.OK.getCode()+ " tout va bien \n"  /*" " + CdaStatus.OK.getDescription() +*/).getBytes());
					clientOut.write("content-type:text/html; charset=UTF-8\n".getBytes());
					clientOut.write("\n".getBytes());
					br = new BufferedReader(new FileReader(file));
					String line;
					while ((line = br.readLine()) != null) {
						clientOut.write(line.getBytes());
					}
				}
				
			}
			
			else if ("get".equalsIgnoreCase(requeteTab[0])) {
				clientOut.write(
						("HTTP/1.1 "+ CdaStatus.OK.getCode()+ " tout va bien \n"  /*" " + CdaStatus.OK.getDescription() +*/).getBytes());
				clientOut.write("content-type:text/html; charset=UTF-8\n".getBytes());
				clientOut.write("\n".getBytes());
				clientOut.write("<html><body><h1>coucou</h1></body></html>".getBytes());
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				this.client.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

}

