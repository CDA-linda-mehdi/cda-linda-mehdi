package cda.http.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class CdaServer extends Thread {
	
	private final int port;
		
	public CdaServer(int p) {
		this.port = p;
	}
	
	@Override
	public void run() {
		
		try {
			ServerSocket ss = new ServerSocket(this.port);
			
			
			Socket client;
			while(true) {
				System.out.println("attente connexion");
				client= ss.accept();
				System.out.println("une connexion cr��e");
				new Thread(new ServeurAuxiliaire2(client)).start();
			}
			
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
}
