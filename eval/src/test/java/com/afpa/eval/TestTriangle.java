package com.afpa.eval;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;

@TestMethodOrder(OrderAnnotation.class)
public class TestTriangle {
	Point a = null, b = null, c = null;
	@Test
	@Order(1)
	void testAjoutTriangle() throws TriangleException {
		try {
			
			Triangle t = new Triangle("nom", a, b, c);
		} catch (Exception e) {
			assertFalse(e instanceof TriangleException);
		}
	}
	
	@Test
	@Order(2)
	void testTriangleDejaExistant() throws TriangleException {
		try {
			Triangle t1 = new Triangle("nom", a, b, c);
			Triangle t2 = new Triangle("nom", a, b, c);
		} catch (Exception e) {
			assertFalse(e instanceof TriangleException);
		}
	}
	
	@Test
	@Order(3)
	void testToString() throws TriangleException {
		try {
			Triangle t = new Triangle("nom", a, b, c);
			a.toString();
		} catch (Exception e) {
			assertFalse(e instanceof TriangleException);
		}
	}
	
	@Test
	@Order(4)
	void testGetA() {
		try {
			Triangle t = new Triangle("nom", a, b, c);
			t.getA();
		} catch (Exception e) {
			assertFalse(e instanceof TriangleException);
		}
	}
	
	@Test
	@Order(5)
	void testGetB() {
		try {
			Triangle t = new Triangle("nom", a, b, c);
			t.getB();
		} catch (Exception e) {
			assertFalse(e instanceof TriangleException);
		}
	}
	
	@Test
	@Order(6)
	void testGetC() {
		try {
			Triangle t = new Triangle("nom", a, b, c);
			t.getA();
		} catch (Exception e) {
			assertFalse(e instanceof TriangleException);
		}
	}
	
	@Test
	@Order(7)
	void testGetList() {
		try {
			Triangle.getListe();
		} catch (Exception e) {
			assertFalse(e instanceof TriangleException);
		}
	}
}
