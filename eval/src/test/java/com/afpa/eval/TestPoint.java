package com.afpa.eval;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

@TestMethodOrder(OrderAnnotation.class)
public class TestPoint {

	@Test
	@Order(1)
	void testAjoutPoint() throws PointException {
		try {
			Point p = new Point("a1", 3, 7);
		} catch (Exception e) {
			assertTrue(e instanceof PointException);
		}

	}

	@Test
	@Order(2)
	void testNomDejaExistant() throws PointException {
		try {
			Point p1 = new Point("a1", 13, 17);
			Point p2 = new Point("a1", 13, 17);
		} catch (Exception e) {
			assertTrue(e instanceof PointException);
		}
	}

	@Test
	@Order(3)
	void testToString() throws PointException {
		try {
			Point a = new Point("a2", 7, 7);
			a.toString();
		} catch (Exception e) {
			assertTrue(e instanceof PointException);
		}
	}

	@Test
	@Order(4)
	void testGetAbs() {
		try {
			Point b = new Point("b2", 7, 7);
			b.getAbs();
		} catch (Exception e) {
			assertTrue(e instanceof PointException);
		}
	}
	
	@Test
	@Order(5)
	void testGetOrd() {
		try {
			Point c = new Point("c2", 7, 7);
			c.getOrd();
		} catch (Exception e) {
			assertTrue(e instanceof PointException);
		}
	}
	
	@Test
	@Order(6)
	void testGetList() {
		try {
			Point.getListe();
		} catch (Exception e) {
			assertTrue(e instanceof PointException);
		}
	}
}
