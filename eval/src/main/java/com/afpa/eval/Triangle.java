package com.afpa.eval;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lombok.Getter;

/**
 * 
 * @author Linda
 * @version 1.0
 *
 */
@Getter
public class Triangle {
	static Logger monLogger = LoggerFactory.getLogger(Point.class);
	private static int id=1;
	private final int monId;
	private String nom;
	private final Point a;
	private final Point b;
	private final Point c;
	private static List<Triangle> liste = new ArrayList<Triangle>();
	
	public Triangle(String name, Point a, Point b, Point c) throws TriangleException {
		this.monId = id;
		this.nom = name;
		this.a = a;
		this.b = b;
		this.c = c;
		boolean avecMemePointTrouve = false;
		for (Triangle triangle : liste) {
			if (triangle.getA().equals(a) || triangle.getB().equals(b) || triangle.getC().equals(c)) {
				 throw new TriangleException();
			} 	
		}
		if (! avecMemePointTrouve) {
			liste.add(this);
			id++;
			monLogger.info("triangle créé et ajouter à la liste.");
		}
	}

	public static List<Triangle> getListe() {
		return liste;
	}

	public static void setListe(List<Triangle> liste) {
		Triangle.liste = liste;
	}

	public Point getA() {
		return a;
	}

	public Point getB() {
		return b;
	}

	public Point getC() {
		return c;
	}

	@Override
	public String toString() {
		return "Triangle " + monId + " nom=" + nom + ", a=" + a + ", b=" + b + ", c=" + c;
	}

	
	
	
	
}
