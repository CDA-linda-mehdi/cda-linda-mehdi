package com.afpa.eval;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Linda
 * @version 1.0
 *
 */
public class Cercle  {

	static Logger monLogger = LoggerFactory.getLogger(Point.class);
	private static int id=1;
	private final int monId;
	private final String nom;
	private Point point;
	private int rayon;
	private static List<Cercle> liste = new ArrayList<Cercle>();
	
	public Cercle(String name, Point point, int rayon) throws CercleException{
		this.monId = id;
		this.nom = name;
		this.point = point;
		this.rayon = rayon;
		boolean avecMemePointOuRayonTrouve = false;
		for (Cercle cercle : liste) {
			if (cercle.getPoint().equals(point) && cercle.getRayon() == rayon) {
				throw new CercleException();
			}
		}
		if (! avecMemePointOuRayonTrouve) {
			liste.add(this);
			id++;
			monLogger.info("cercle créé et ajouter à la liste.");
		}
	}

	public Point getPoint() {
		return point;
	}

	public void setPoint(Point point) {
		this.point = point;
	}

	public int getRayon() {
		return rayon;
	}

	public void setRayon(int rayon) {
		this.rayon = rayon;
	}

	public static List<Cercle> getListe() {
		return liste;
	}

	public static void setListe(List<Cercle> liste) {
		Cercle.liste = liste;
	}

	public int getMonId() {
		return monId;
	}

	public String getNom() {
		return nom;
	}

	@Override
	public String toString() {
		return "Cercle " + monId + ", nom=" + nom + ", point=" + point + ", rayon=" + rayon;
	}
	
	
	
	
}
