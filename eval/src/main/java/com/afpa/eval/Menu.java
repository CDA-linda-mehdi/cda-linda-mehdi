package com.afpa.eval;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Linda
 * @version 1.0
 *
 */
public class Menu {
	static Logger monLogger = LoggerFactory.getLogger(Point.class);

	public static void main(String[] args) throws PointException, TriangleException, CercleException {
		ArrayList<Point> listPoint = new ArrayList<Point>();
		ArrayList<Triangle> listTriangle = new ArrayList<Triangle>();
		ArrayList<Cercle> listCercle = new ArrayList<Cercle>();
		Point point = null;
		Triangle triangle = null;
		
		Scanner sc = new Scanner(System.in);
		
		boolean b = true;
		String str;
		String str1;
		String str2;
		String str3;
		int rep;
		int rep1;
		int rep2;
		int rep3;
		
		while (b) {
			monLogger.info("Entrée dans le menu");
			System.out.println("Veuillez choisir une des options ci-dessous : ");
			System.out.println("1 - Créer des point : ");
			System.out.println("2 - Créer des triangles : ");
			System.out.println("3 - Créer des cercles : ");
			System.out.println("4 - Lister les points par ordre croissant : ");
			System.out.println("5 - Lister les triangles par ordre croissant des surfaces : ");
			System.out.println("6 - Lister les cercles par ordre croissant des surfaces : ");
			System.out.println("7 - Sortir du programme");
			rep = sc.nextInt();
			sc.nextLine();
			
			switch (rep) {
			case 1 :
				
				System.out.println("Indiquer le nom du point : ");
				str = sc.nextLine();
				System.out.println("Indiquer l'abscisse du point : ");
				rep1 = sc.nextInt();
				System.out.println("Indiquer l'ordonnee du point : ");
				rep2 = sc.nextInt();
				point = new Point(str,rep1,rep2);
				System.out.println(point);
				listPoint.add(point);
				break;
			case 2 :
				System.out.println("Indiquer le nom du triangle : ");
				str = sc.nextLine();
				System.out.println("Indiquer le nom du premier point : ");
				str1 = sc.nextLine();
				System.out.println("Indiquer le nom du deuxième point : ");
				str2 = sc.nextLine();
				System.out.println("Indiquer le nom du troisième point : ");
				str3 = sc.nextLine();
				Point pointA = null, pointB = null, pointC = null;
				for (Point point1 : listPoint) {
					if (point1.getNom().equalsIgnoreCase(str1)) {
						pointA = point1;
					}
				}
				for (Point point2 : listPoint) {
					if (point2.getNom().equalsIgnoreCase(str2)) {
						pointB = point2;
					}
				}
				for (Point point3 : listPoint) {
					if (point3.getNom().equalsIgnoreCase(str3)) {
						pointC = point3;
					}
				}
				triangle = new Triangle(str, pointA, pointB, pointC);
				listTriangle.add(triangle);
				break;
			case 3 :
				System.out.println("Indiquer le nom du cercle : ");
				str = sc.nextLine();
				System.out.println("Indiquer le nom du point pour positionner le cerlce : ");
				str1 = sc.nextLine();
				System.out.println("Indiquer la taille du rayon du cercle : ");
				rep1 = sc.nextInt();
				Point pointD = null;
				int rayon;
				for (Point point4 : listPoint) {
					if (point4.getNom().equalsIgnoreCase(str1)) {
						pointD = point4;
					}
				}
				Cercle cercle = new Cercle(str, pointD, rep1);
				break;
			case 4 :
				listPoint = (ArrayList<Point>) Point.getListe();
				System.out.println(listPoint);
				break;
			case 5 :
				listTriangle = (ArrayList<Triangle>) Triangle.getListe();
				System.out.println(listTriangle);
				break;
			case 6 :
				listCercle = (ArrayList<Cercle>) Cercle.getListe();
				System.out.println(listCercle);
				break;
			case 7 :
				System.out.println("Sortie du programme");
				b = false;
				monLogger.info("Sortie du menu");
				break;
				default :
					System.out.println("Vous n'avez pas saisie un choix de la liste !\n");
					
			}
		}
	}
}
