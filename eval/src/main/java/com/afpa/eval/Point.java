package com.afpa.eval;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



/**
 * 
 * @author Linda
 * @version 1.0
 *
 */

public class Point {
	
	static Logger monLogger = LoggerFactory.getLogger(Point.class);
	private static int id=1;
	private final int monId;
	private final String nom;
	private final int abs;
	private final int ord;
	private static List<Point> liste = new ArrayList<Point>();

	public Point(String name, int a, int b)  throws PointException{
		this.monId = id;
		this.nom = name;
		this.abs = a;
		this.ord = b;
		boolean avecMemeNomTrouve = false;
		for (Point point : liste) {
			if (point.getNom().equalsIgnoreCase(name)) {
				 throw new PointException();
			} 	
		}
		if (! avecMemeNomTrouve) {
			liste.add(this);
			id++;
			monLogger.info("point créé et ajouter à la liste.");
		}
	}
	

	public String getNom() {
		return nom;
	}

	public int getAbs() {
		return abs;
	}


	public int getOrd() {
		return ord;
	}

	public static List<Point> getListe() {
		return liste;
	}

	@Override
	public String toString() {
		return "Point " + monId + " nom=" + nom + ", abs=" + abs + ", ord=" + ord + "\n";
	}
  
}