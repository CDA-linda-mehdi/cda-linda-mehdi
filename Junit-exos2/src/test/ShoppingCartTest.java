package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import main.Product;
import main.ProductNotFoundException;
import main.ShoppingCart;

class ShoppingCartTest {
	
	ShoppingCart cart;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
		cart = new ShoppingCart();
	}
 
	@AfterEach
	void tearDown() throws Exception {
	}
	

	@Test
	void testAddItem() {
		Product a = new Product("Pomme", 2.5);
		Product b = new Product("Mangue", 7.5);
		cart.addItem(a);
		cart.addItem(b);
		assertEquals(10, 0, cart.getBalance());
	}


	@Test
	void testRemoveItem() throws ProductNotFoundException {
		Product a = new Product("Cl�mentine", 2.5);
		Product b = new Product("Ananas", 1.5);
		cart.addItem(a);
		cart.addItem(b);
		cart.removeItem(a);
		assertEquals("1 produit retir� sur 2", 1, cart.getItemCount());
	}

	@Test
	void testEmpty() {
		Product a = new Product("Olives", 3.5);
		Product b = new Product("Salade", 1.5);
		cart.addItem(a);
		cart.addItem(b);
		cart.empty();
		assertEquals("Liste vid� de deux articles", 0, cart.getItemCount());
	}

	@Test
	void testWhenCreateCartHas0Item() {
		ShoppingCart cart2 = new ShoppingCart();
		assertEquals(0, cart2.getItemCount());
		System.out.println("Nombre d'items � z�ro");
	}
	
	@Test
	void testAddItemMustSumTheBalance() {
		Product a = new Product("Pomme", 2.5);
		Product b = new Product("Mangue", 7.5);
		cart.addItem(a);
		cart.addItem(b);
		assertEquals(10, 0, cart.getBalance());
	}
	
	@Test
	void testRemoveNotItemFound() {
		Throwable e = null;
		try {
			Product a = new Product("Cl�mentine", 2.5);
			Product b = new Product("Ananas", 1.5);
			Product c = new Product("Past�que", 3.5);
			cart.addItem(a);
			cart.addItem(b);
			cart.removeItem(c);
			fail("normalement ne doit jamais arriver ici !!!");
		} catch (Throwable ex){ 
			e = ex;
		}
		assertTrue(e instanceof ProductNotFoundException);
	}

}
