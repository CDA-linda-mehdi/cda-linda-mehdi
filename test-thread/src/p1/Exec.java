package p1;

import java.util.Date;

public class Exec {

	public static void main(String[] args) throws Exception {
		
		MonPremierThread m1 = new MonPremierThread("coucou");
		MonPremierThread m2 = new MonPremierThread("kiki");
		MonPremierThread m3 = new MonPremierThread("loulou");
		System.out.println("------------- "+new Date());
		Thread t2 = new Thread(m2);
		Thread t3 = new Thread(m3);
		Thread t1 = new Thread(m1);
//		m1.ajouterUnThreadAAttendre(t2);
//		m1.ajouterUnThreadAAttendre(t3);
//		t2.start();
//		t3.start();
		t1.start();
		t1.join();
		t2.join();
		t3.join();
		System.out.println(t1.getState());
		System.out.println("++++++++++++++++ "+new Date());
	}
}
