package p1;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MonPremierThread implements Runnable {
	
	private final String nom;
	private List<Thread> threadAAttendre;
	
	public MonPremierThread(String nom) {
		this.threadAAttendre = new ArrayList<>();
		this.nom = nom;
	}
	
	public void compter() {
		for(int i= 0; i<40; i++) {
			System.out.println(this.nom + " "+i+" "+new Date().getTime());
		}
	}
	public void ajouterUnThreadAAttendre(Thread m) {
		this.threadAAttendre.add(m);
	}
	public void run() {
		this.threadAAttendre.forEach(x->{
			try {
				x.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		});
		this.compter();
	}
}
