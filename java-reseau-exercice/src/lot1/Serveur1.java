package lot1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Random;

public class Serveur1 extends IOException {
	static String nombreClient = "-1";
	static Integer random;
	static String ChaineFinale = "-1";

	// methode pour le choix d'un nombre aleatoire avec gestion d'exception
	private static int nombreAleatoire(int min, int max) {
		if (min >= max) {
			throw new IllegalArgumentException("le maximum doit etre plus grand que le minimum");
		}
		Random ramdomOrdi = new Random();

		return ramdomOrdi.nextInt((max - min) + 1) + min;
	}

	/************* MAIN ***********/

	public static void main(String[] args) throws IOException {

		random = nombreAleatoire(0, 100);

		System.out.println(random);
		// le port 8084 est ouvert
		// je branche mon tuyau c1 au port 8085
		ServerSocket s1 = new ServerSocket(8085);

		Socket c1 = s1.accept();

		/******************
		 * ENVOI MESSAGE1 AU CLIENT TELNET PAR LE SERVEUR3
		 **************/
		// creation d'un objet OuputStream pour ecrire le message1
		OutputStream os1 = c1.getOutputStream();
		PrintWriter pw1 = new PrintWriter(os1);
		pw1.println("connexion r�ussie");
		pw1.flush();

		/******************
		 * ENVOI MESSAGE2 AU CLIENT TELNET PAR LE SERVEUR3
		 **************/
		// le serveur va envoyer le message2 "Tapez votre nom"...

		OutputStream os2 = c1.getOutputStream();
		PrintWriter pw2 = new PrintWriter(os2);
		pw2.println("Entrez votre NOM : ");
		pw2.flush();

		/******************
		 * LECTURE MESSAGE3 DU CLIENT TELNET AU SERVEUR3
		 **************/
		// lecture du message3 : nom du client1
		InputStream is1 = c1.getInputStream();
		BufferedReader br1 = new BufferedReader(new InputStreamReader(is1));
		String message1 = br1.readLine();

		String NomClient = new String(message1);

		System.err.println("<" + NomClient + ">");
		/******************
		 * ECRITURE MESSAGE4 AU CLIENT TELNET PAR LE SERVEUR3
		 **************/

		// le serveur va envoyer le message4 "Bienvenue Tapez votre nom"...

		String Str4_1 = "BIENVENUE " + "  " + NomClient + " CHOISISSEZ UN NOMBRE ENTRE 0 ET 100 \n";
		OutputStream os11 = c1.getOutputStream();
		PrintWriter pw3 = new PrintWriter(os11);
		pw2.print(Str4_1);
		pw2.flush();

		/******************
		 * LECTURE MESSAGE5 DU CLIENT TELNET AU SERVEUR3
		 **************/
		// lecture du message5 : nombre entre entre 0 et 100

		while (Integer.parseInt(ChaineFinale) != random) {
			InputStream is2 = c1.getInputStream();

			BufferedReader br2 = new BufferedReader(new InputStreamReader(is1));

			String message2 = br2.readLine();

			System.out.println(nombreClient);

			nombreClient = new String(message2);

			System.err.println("length of nombre " + nombreClient.length());

			for (int i = 0; i < nombreClient.length(); i++) {

				Character c = nombreClient.charAt(i);

				System.err.println("c == " + c);
				if (Character.isDigit(c) == true) {

					ChaineFinale += c;
					System.out.println(ChaineFinale);

				} else {

					break;
				}
			}
			System.err.println(nombreClient);

			if (Integer.parseInt(nombreClient) < random) {

				OutputStream os3 = c1.getOutputStream();
				PrintWriter pw4 = new PrintWriter(os3);
				pw3.print(" C'EST PLUS : ");
				pw3.flush();

			} else if (Integer.parseInt(nombreClient) > random) {

				OutputStream os3 = c1.getOutputStream();
				PrintWriter pw5 = new PrintWriter(os3);
				pw5.println(" C'EST MOINS : ");
				pw5.flush();

			}
			if (Integer.parseInt(nombreClient) == random) {
				OutputStream os3 = c1.getOutputStream();
				PrintWriter pw6 = new PrintWriter(os3);
				pw6.println("GAGNE  " + " " + NomClient);
				pw6.flush();

			}
		}
		c1.close();
		s1.close();
	}
}