package lot1;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class Serveur {

	public static void main(String[] args) throws Exception {

		ServerSocket srv = new ServerSocket(8096);
		int nbrSrv = Tools.nombreAleatoire();
		System.out.println(nbrSrv);
		Socket clt = srv.accept();
		
		OutputStream out = clt.getOutputStream();
		out.write("*******************************\r\n*** JEU DU PLUS ET DU MOINS ***\r\n*******************************".getBytes());
		out.write("\r\nConnexion reussie !".getBytes());
		out.write("\r\n\nSaisissez votre nom : ".getBytes());
		
		BufferedReader reader = new BufferedReader(new InputStreamReader(clt.getInputStream()));
		String nomClt = reader.readLine();
		out.write(("Bienvenue " + nomClt + " choisissez un nombre entre 0 et 100\n").getBytes());
		
		int nbrClt = reader.read();
		boolean test = true;
		
		while (test) {
			out.write(("Choisissez un nombre entre 0 et 100 : ").getBytes());
			if (nbrClt > nbrSrv) {
				out.write(("+").getBytes());
			} else if (nbrClt < nbrSrv) {
				out.write(("-").getBytes());
			} else if (nbrClt == nbrSrv) {
				out.write(("Bravo " +nomClt).getBytes());
				test = false;
			}
		}
		
		//out.write(nbrClt);
		Thread.sleep(10000);
		
		//srv.close();
	}

}
