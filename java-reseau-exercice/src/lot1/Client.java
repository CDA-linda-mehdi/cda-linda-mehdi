package lot1;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;

public class Client {

	public static void main(String[] args) throws IOException {
		Socket clt = new Socket();
		SocketAddress host = new InetSocketAddress("localhost", 8096);
		clt.connect(host);
	}

}
