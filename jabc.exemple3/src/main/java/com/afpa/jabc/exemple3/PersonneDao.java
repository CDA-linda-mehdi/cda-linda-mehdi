package com.afpa.jabc.exemple3;
import java.util.List;

public interface PersonneDao {
	Personne save(Personne personne);
	void remove(Personne personne);
	Personne update(Personne personne);
	Personne findById(int id);
	List<Personne> getAll();
}

