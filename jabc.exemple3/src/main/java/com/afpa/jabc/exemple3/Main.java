package com.afpa.jabc.exemple3;

public class Main {
	public static void main(String args []) {
		PersonneDao personneDaoImpl = new PersonneDaoImpl();
		Personne personne = new Personne (5,"Wick","John");
		Personne insertedPersonne = personneDaoImpl.save(personne);
		if (insertedPersonne != null)
			System.out.println("personne numéro " + insertedPersonne.
					getNum() + " a été inséré");
		else
			System.out.println("problème d'insertion");
	}
}