package com.afpa.jabc.exemple3;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@RequiredArgsConstructor
public class Personne {
	private int num;
	@NonNull
	private String nom;
	@NonNull
	private String prenom;

}
