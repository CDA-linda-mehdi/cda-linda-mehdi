package com.afpa.jabc.exemple3;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class MyConnection {

	private static String url = "jdbc:postgresql://localhost:5432/jdbc-test";
	private static String utilisateur = "postgres";
	private static String motDePasse = "Linda59*";
	private static Connection connexion = null;

	public static void stop() {
		if (connexion != null) {
			try {
				connexion.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	private MyConnection() {
		try {
			connexion = DriverManager.getConnection( url, utilisateur,
					motDePasse );
		} catch ( Exception e ) {
			e.printStackTrace();
		}
	}
	public static Connection getConnection() {
		if (connexion == null) {
			new MyConnection();
		}
		return connexion;
	}

}
