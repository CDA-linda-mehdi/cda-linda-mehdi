package com.afpa.jabc.exemple3;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class PersonneDaoImpl implements PersonneDao {

	
	public void remove(Personne personne) {
		// TODO Auto-generated method stub

	}

	public Personne update(Personne personne) {
		// TODO Auto-generated method stub
		return null;
	}


	public List<Personne> getAll() {
		// TODO Auto-generated method stub
		return null;
	}

	public Personne save(Personne personne) {
		Connection c = MyConnection.getConnection();
		if (c != null) {
			try {
				PreparedStatement ps = c.prepareStatement("insert into personne (num,nom,prenom) values (?,?,?); ", PreparedStatement.RETURN_GENERATED_KEYS);
				ps.setInt(1, 5);
				ps.setString(2, personne.getNom());
				ps.setString(3, personne.getPrenom());
				ps.executeUpdate();
				ResultSet resultat = ps.getGeneratedKeys();
				if (resultat.next()) {
					personne.setNum(resultat.getInt(1));
					return personne;
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	public Personne findById(int id) {
		Connection c = MyConnection.getConnection();
		if (c != null) {
			try {
				PreparedStatement ps = c.prepareStatement("select * from personne where num = ?; ");
				ps.setInt(1, id);
				ResultSet r =ps.executeQuery();
				if (r.next())
					return new Personne(r.getInt("num"),r.getString("nom"),r.getString("prenom"));
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
}