package com.afpa.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ResponsableDto {

	private String nom;
}
