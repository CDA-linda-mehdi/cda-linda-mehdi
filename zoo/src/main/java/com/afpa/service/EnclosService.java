package com.afpa.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.afpa.dao.EnclosDao;
import com.afpa.dto.EnclosDto;
import com.afpa.entite.Enclos;

public class EnclosService {

	EnclosDao ed = new EnclosDao();
	EnclosDto enclosdto = new EnclosDto();
	
	public void ajouter(String nom) {
		Enclos e = new Enclos();
		e.setNom(nom);
		ed.add(e);
	}
	
	public EnclosDto findId(int id) {
		ed.find(id);
		enclosdto.setNom(ed.find(id).getNom());
		return enclosdto;
	}
	
	public Collection<EnclosDto> afficherAll() {
		List<EnclosDto> listenclosdto = new ArrayList<>();
		for (Enclos enclos : ed.fndAllNamedQuery("listEnclos")) {
			enclosdto.setNom(enclos.getNom());
			listenclosdto.add(enclosdto);
		}
		return listenclosdto;
	}
	
	public void update(int id, String nom) {
		Enclos e = ed.find(id);
		e.setNom(nom);
		ed.update(e);
	}
	
	public void remove(int id) {
		ed.remove(id);
	}
}
