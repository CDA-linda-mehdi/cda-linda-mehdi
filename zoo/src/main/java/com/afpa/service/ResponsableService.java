package com.afpa.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.afpa.dao.ResponsableDao;
import com.afpa.dto.ResponsableDto;
import com.afpa.entite.Responsable;

public class ResponsableService {

	ResponsableDao rd = new ResponsableDao();
	ResponsableDto resdto = new ResponsableDto();
	
	public void ajouter(String nom) {
		Responsable r = new Responsable();
		r.setNom(nom);
		rd.add(r);
	}
	
	public ResponsableDto findId(int id) {
		rd.find(id);
		resdto.setNom(rd.find(id).getNom());
		return resdto;
	}
	
	public Collection<ResponsableDto> afficheAll() {
		List<ResponsableDto> listresdto = new ArrayList<>();
		for (Responsable responsable : rd.fndAllNamedQuery("listeResponsable")) {
			resdto.setNom(responsable.getNom());
			listresdto.add(resdto);
		}
		return listresdto;
	}
	
	public void update(int id, String nom) {
		Responsable r = new Responsable();
		r.setNom(nom);
		rd.update(r);
	}
	
	public void remove(int id) {
		rd.remove(id);
	}
}
