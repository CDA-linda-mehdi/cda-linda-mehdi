package com.afpa.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.afpa.dao.AlimentDao;
import com.afpa.dto.AlimentDto;
import com.afpa.dto.RaceDto;
import com.afpa.entite.Aliment;

public class AlimentService {

	AlimentDao ad = new AlimentDao();
	AlimentDto alimentDto = new AlimentDto();
	
	public void ajouter(String nom) {
		Aliment a = new Aliment();
		a.setNom(nom);
		ad.add(a);
	}
	
	public AlimentDto findId(int id) {
		ad.find(id);
		alimentDto.setNom(ad.find(id).getNom());
		return alimentDto;
	}
	
	public Collection<AlimentDto> afficherAll() {
		List<AlimentDto> listalimentdto = new ArrayList<>();
		for (Aliment aliment : ad.fndAllNamedQuery("listeAliment")) {
			alimentDto.setNom(aliment.getNom());
			listalimentdto.add(alimentDto);
		}
		return listalimentdto;
	}
	
	public void update(int id, String nom) {
		Aliment a = new Aliment();
		a.setNom(nom);
		ad.update(a);
	}
	
	public void remove(int id) {
		ad.remove(id);
	}
}
