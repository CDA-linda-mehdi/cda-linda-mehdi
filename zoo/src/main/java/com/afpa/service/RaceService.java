package com.afpa.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.afpa.dao.RaceDao;
import com.afpa.dto.RaceDto;
import com.afpa.entite.Race;

public class RaceService {

	RaceDao rd = new RaceDao();
	RaceDto raceDto = new RaceDto();
	
	public void ajouter(String nom) {
		Race r = new Race();
		r.setNom(nom);
		rd.add(r);
	}
	
	public RaceDto findId(int id) {
		rd.find(id);
		raceDto.setNom(rd.find(id).getNom());
		return raceDto;
	}
	
	public Collection<RaceDto> afficherAll() {
		List<RaceDto> listracedto = new ArrayList<>();
		for (Race race : rd.fndAllNamedQuery("listeRace")) {
			raceDto.setNom(race.getNom());
			listracedto.add(raceDto);
		}
		return listracedto;
	}
	
	public void update(int id, String nom) {
		Race r = rd.find(id);
		r.getNom();
		rd.update(r);
	}
	
	public void remove(int id) {
		rd.remove(id);
	}
}
