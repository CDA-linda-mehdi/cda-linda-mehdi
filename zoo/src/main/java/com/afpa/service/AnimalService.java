package com.afpa.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.afpa.dao.AnimalDao;
import com.afpa.dto.AnimalDto;
import com.afpa.entite.Animal;

public class AnimalService {

	AnimalDao ad = new AnimalDao();
	AnimalDto animaldto = new AnimalDto();
	
	public void ajouter(String nom) {
		Animal a = new Animal();
		a.setNom(nom);
		ad.add(a);
	}
	
	public AnimalDto findId(int id) {
		ad.find(id);
		animaldto.setNom(ad.find(id).getNom());
		return animaldto;
	}
	
	public Collection<AnimalDto> afficherAll() {
		List<AnimalDto> listanidto = new ArrayList<>();
		for (Animal animal : ad.fndAllNamedQuery("listAnimaux")) {
			animaldto.setNom(animal.getNom());
			listanidto.add(animaldto);
		}
		return listanidto;
	}
	
	public void update(int id, String nom) {
		Animal a = ad.find(id);
		a.setNom(nom);
		ad.update(a);
	}
	
	public void remove(int id) {
		ad.remove(id);
	}
}

