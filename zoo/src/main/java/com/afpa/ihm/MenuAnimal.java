package com.afpa.ihm;

import java.util.Scanner;

import com.afpa.service.AnimalService;

public class MenuAnimal {

public static AnimalService as = new AnimalService();
	
	public static void menu() {
		
		boolean bool = true;
		
		while(bool) {
			System.out.println("***********************************");
			System.out.println("*********** Menu Animal ***********");
			System.out.println("***********************************");
			System.out.println("1 - Ajouter un animal");
			System.out.println("2 - Afficher la liste des animaux");
			System.out.println("3 - Trouver un animal par son ID");
			System.out.println("4 - Mettre à jour un animal");
			System.out.println("5 - Supprimer un animal");
			System.out.println("6 - Fermer le menu des animaux");
			
			System.out.println("Veuillez indiquer votre choix");
			Scanner sc = new Scanner(System.in);
			String choix = sc.next();
			
			switch (choix) {
			case "1":
				System.out.println("Ajouter un animal");
				System.out.println("Entrez le nom de l'animal");
				String nom = sc.next();
				as.ajouter(nom);
				break;
				
			case "2":
				System.out.println("Afficher la liste des animaux");
				as.afficherAll().stream().forEach(System.out::println);
				break;
				
			case "3":
				System.out.println("Trouver un animal par son ID");
				System.out.println("Entrez l'ID de l'animal : ");
				int id = sc.nextInt();
				System.out.println(as.findId(id));
				break;
				
			case "4":
				System.out.println("Mettre à jour un animal");
				System.out.println("Entrez l'ID de l'animal à mettre à jour : ");
				int id2 = sc.nextInt();
				System.out.println("Entrez le nouveau nom de l'animal : ");
				String nom2 = sc.next();
				as.update(id2, nom2);
				break;
			case"5":
				System.out.println("5 - Supprimer un animal");
				System.out.println("Entrez l'ID de l'animal à supprimer : ");
				int id3 = sc.nextInt();
				as.remove(id3);
				break;
			case"6":
				System.out.println("Programme fermé");
				bool = false;
				default:
					System.out.println("L'option choisie n'existe pas !");
					break;
			}
		}
	}
}
