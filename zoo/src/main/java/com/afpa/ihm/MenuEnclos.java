package com.afpa.ihm;

import java.util.Scanner;

import com.afpa.service.EnclosService;

public class MenuEnclos {

public static EnclosService es = new EnclosService();
	
	public static void menu() {
		
		boolean bool = true;
		
		while(bool) {
			System.out.println("***********************************");
			System.out.println("*********** Menu Enclos ***********");
			System.out.println("***********************************");
			System.out.println("1 - Ajouter un enclos");
			System.out.println("2 - Afficher la liste des enclos");
			System.out.println("3 - Trouver un enclos par son ID");
			System.out.println("4 - Mettre à jour un enclos");
			System.out.println("5 - Supprimer un enclos");
			System.out.println("6 - Fermer le menu des enclos");
			
			System.out.println("Veuillez indiquer votre choix");
			Scanner sc = new Scanner(System.in);
			String choix = sc.next();
			
			switch (choix) {
			case "1":
				System.out.println("Ajouter un enclos");
				System.out.println("Entrez le nom de l'enclos");
				String nom = sc.next();
				es.ajouter(nom);
				break;
				
			case "2":
				System.out.println("Afficher la liste des enclos");
				es.afficherAll().stream().forEach(System.out::println);
				break;
				
			case "3":
				System.out.println("Trouver un enclos par son ID");
				System.out.println("Entrez l'ID de l'enclos : ");
				int id = sc.nextInt();
				System.out.println(es.findId(id));
				break;
				
			case "4":
				System.out.println("Mettre à jour un enclos");
				System.out.println("Entrez l'ID de l'enclos à mettre à jour : ");
				int id2 = sc.nextInt();
				System.out.println("Entrez le nouveau nom de l'enclos : ");
				String nom2 = sc.next();
				es.update(id2, nom2);
				break;
			case"5":
				System.out.println("5 - Supprimer un enclos");
				System.out.println("Entrez l'ID de l'enclos à supprimer : ");
				int id3 = sc.nextInt();
				es.remove(id3);
				break;
			case"6":
				System.out.println("Programme fermé");
				bool = false;
				default:
					System.out.println("L'option choisie n'existe pas !");
					break;
			}
		}
	}
}
