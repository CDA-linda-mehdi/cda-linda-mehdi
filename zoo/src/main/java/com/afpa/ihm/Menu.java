package com.afpa.ihm;

import java.util.Scanner;

public class Menu {

	public static void main(String[] args) {
		
		boolean bool = true;
		
		while(bool) {
			System.out.println("**********************************");
			System.out.println("********* Menu principal *********");
			System.out.println("**********************************");
			System.out.println("1 - Aller vers le menu des responsable");
			System.out.println("2 - Aller vers le menu des animaux");
			System.out.println("3 - Aller vers le menu des races");
			System.out.println("4 - Aller vers le menu des enclos");
			System.out.println("5 - Aller vers le menu des aliments");
			System.out.println("6 - Fermer le menu");
			
			Scanner sc = new Scanner(System.in);
			String choix = sc.next();
			
			switch (choix) {
			case "1":
				MenuResponsable.menu();
				break;
			case "2":
				MenuAnimal.menu();
				break;
			case "3":
				MenuRace.menu();
				break;
			case "4":
				MenuEnclos.menu();
				break;
			case "5":
				MenuAliment.menu();
				break;
			case "6":
				System.out.println("Menu fermé !");
				bool = false;
				break;
				
				default:
					break;
			}
		}

	}

}
