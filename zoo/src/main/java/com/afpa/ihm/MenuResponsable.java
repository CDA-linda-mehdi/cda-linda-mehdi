package com.afpa.ihm;

import java.util.Scanner;

import com.afpa.service.ResponsableService;

public class MenuResponsable {

	public static ResponsableService rs = new ResponsableService();
	
	public static void menu() {
		
		boolean bool = true;
		
		while(bool) {
			System.out.println("**********************************");
			System.out.println("******** Menu Responsable ********");
			System.out.println("**********************************");
			System.out.println("1 - Ajouter un responsable");
			System.out.println("2 - Afficher la liste des responsables");
			System.out.println("3 - Trouver un responsable par son ID");
			System.out.println("4 - Mettre à jour un responsable");
			System.out.println("5 - Supprimer un responsable");
			System.out.println("6 - Fermer le menu des responsables");
			
			System.out.println("Veuillez indiquer votre choix");
			Scanner sc = new Scanner(System.in);
			String choix = sc.next();
			
			switch (choix) {
			case "1":
				System.out.println("Ajouter un responsable");
				System.out.println("Entrez le nom du responsable");
				String nom = sc.next();
				rs.ajouter(nom);
				break;
				
			case "2":
				System.out.println("Afficher la liste des responsables");
				rs.afficheAll().stream().forEach(System.out::println);
				break;
				
			case "3":
				System.out.println("Trouver un responsable par son ID");
				System.out.println("Entrez l'ID du responsable : ");
				int id = sc.nextInt();
				System.out.println(rs.findId(id));
				break;
				
			case "4":
				System.out.println("Mettre à jour un responsable");
				System.out.println("Entrez l'ID du responsable à mettre à jour : ");
				int id2 = sc.nextInt();
				System.out.println("Entrez le nouveau nom du responsable : ");
				String nom2 = sc.next();
				rs.update(id2, nom2);
				break;
			case"5":
				System.out.println("5 - Supprimer un responsable");
				System.out.println("Entrez l'ID du responsable à supprimer : ");
				int id3 = sc.nextInt();
				rs.remove(id3);
				break;
			case"6":
				System.out.println("Programme fermé");
				bool = false;
				default:
					System.out.println("L'option choisie n'existe pas !");
					break;
			}
		}
	}
}
