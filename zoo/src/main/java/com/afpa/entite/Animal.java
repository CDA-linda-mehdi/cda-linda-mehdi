package com.afpa.entite;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
@Table(name = "t_animal")
@NamedQuery(name = "listAnimaux", query = "SELECT a FROM Animal a")
public class Animal {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	@JoinColumn(name = "nom")
	private String nom;
	
	@ManyToOne
	@JoinColumn(name = "race")
	private Race race;
	
	@ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
	@JoinTable(name = "t_Animal_Aliment", joinColumns = { @JoinColumn(name = "id_animal")}, 
		inverseJoinColumns = { @JoinColumn(name = "id_aliment")})
	private Set<Aliment> aliments;
	
	@ManyToOne
	@JoinColumn(name = "enclos")
	private Enclos enclos;
	
	@ManyToOne
	@JoinColumn(name = "responsable")
	private Responsable responsable;
}
