package com.afpa.entite;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
@Table(name = "t_responsable")
@NamedQueries({
	@NamedQuery(name = "listeResponsable", query = "SELECT r FROM Responsable r"),
	@NamedQuery(name = "listeResponsableAvecAliment", query = "SELECT r FROM Responsable r")
})
@Entity
public class Responsable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	@Column(name = "name")
	private String nom;
	
	@Embedded
	private Enclos enclos;
	
	@OneToMany(cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY, mappedBy = "responsable")
	private Set<Animal> animaux;
	
	@ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
	@JoinTable(name = "t_Responsable_Aliment", joinColumns = { @JoinColumn(name = "id_responsable")}, 
		inverseJoinColumns = { @JoinColumn(name = "id_aliment")})
	private Set<Aliment> aliments;

}
