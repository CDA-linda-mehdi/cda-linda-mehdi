package com.afpa.dao;

import java.lang.reflect.ParameterizedType;
import java.util.Collection;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AbstractDao<T> implements IDao<T> {
	private static Logger monLogger = LoggerFactory.getLogger(ResponsableDao.class);
	private static Logger monLogger2 = LoggerFactory.getLogger(RaceDao.class);

	private EntityManagerFactory factory = null;
	private final Class<T> clazz;
	
	@SuppressWarnings("unchecked")
	public AbstractDao() {
		this.clazz = (Class<T>) ((ParameterizedType) this.getClass().getGenericSuperclass())
				.getActualTypeArguments()[0];
		this.factory = Persistence.createEntityManagerFactory("myBase");
	}
	
	public void close() {
		if (this.factory != null) {
			this.factory.close();
		}
	}
	
	public EntityManager newEntityManager() {
		EntityManager em = this.factory.createEntityManager();
		em.getTransaction().begin();
		return em;
	}
	
	public void closeEntityManager(EntityManager em) {
		if (em != null) {
			if (em.isOpen()) {
				EntityTransaction t = em.getTransaction();
				if (t.isActive()) {
					try {
						t.rollback();
					} catch (PersistenceException e) {
						e.printStackTrace();
					}
				}
				em.close();
			}
		}
	}
	
	public T add(T entity) {
		EntityManager em = null;
		try {
			em = newEntityManager();
			em.persist(entity);
			em.getTransaction().commit();
			return (entity);
		} finally {
			closeEntityManager(em);
		}
	}
	public T find(int id) {
		EntityManager em = null;
		try {
			em = newEntityManager();
			return em.find(this.clazz, id);
		} finally {
			closeEntityManager(em);
		}
	}
	public T update(T entity) {
		EntityManager em = null;
		try {
			em = newEntityManager();
			entity = em.merge(entity);
			em.getTransaction().commit();
		} finally {
			closeEntityManager(em);
		}
		return entity;
	}
	
	public Collection<T> fndAllNamedQuery(String query) {
		EntityManager em = null;
		try {
			em = newEntityManager();
			TypedQuery<T> q = em.createNamedQuery(query, this.clazz);
			return q.getResultList();
		} finally {
			closeEntityManager(em);
		}
	}
	public void remove(int id) {
		EntityManager em = null;
		try {
			em = newEntityManager();
			T entity = em.find(this.clazz, id);
			if (entity != null) {
				em.remove(entity);
			}
			em.getTransaction().commit();
		} finally {
			closeEntityManager(em);
		}
		
	}
	
}
