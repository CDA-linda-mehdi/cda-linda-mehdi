package com.afpa.dao;

import java.util.List;

import com.afpa.entite.Responsable;

public interface IResponsable {

	public List<Responsable> listerResponsableEtLeursAliments(Responsable responsable);
	
}
