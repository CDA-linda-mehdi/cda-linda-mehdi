package com.afpa.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.util.Collection;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import com.afpa.dao.AnimalDao;
import com.afpa.entite.Aliment;
import com.afpa.entite.Animal;

@TestMethodOrder(value = OrderAnnotation.class)
public class AnimalDaoTest {

	static AnimalDao dao;
	
	@BeforeAll
	public static void beforAll() {
		dao = new AnimalDao();
	}
	
	@AfterAll
	public static void afterAll() {
		dao.close();
	}
	
	private static Animal animal;
	
	@Test
	@Order(1)
	public void ajoutAliment() {
		Animal a = Animal.builder().id(1).nom("test").build();
		a = dao.add(a);
		assertNotNull(a);
		assertNotEquals(0, a.getId());
		animal = a;
	}
	
	@Test
	@Order(2)
	public void miseAJourAnimal() {
		animal.setId(2);
		dao.update(animal);
		Animal a = dao.find(animal.getId());
		assertNotNull(a);
		assertEquals(2, animal.getId());
	}
	
	@Test
	@Order(3)
	public void listerAnimal() {
		Collection<Animal> aliments = dao.fndAllNamedQuery("listAnimaux");
		aliments.stream().forEach(System.out::println);
	}
	
	@Test
	@Order(4)
	public void trouverAnimalById() {
		Animal a = dao.find(animal.getId());
		assertNotNull(a);
		assertEquals(animal.getId(), a.getId());
	}
	
	@Test
	@Order(5)
	public void supprimerAnimal() {
		dao.remove(animal.getId());
		Animal a = dao.find(animal.getId());
		assertNull(a);
	}
}
