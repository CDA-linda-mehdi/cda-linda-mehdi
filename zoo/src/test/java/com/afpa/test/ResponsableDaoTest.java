package com.afpa.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.util.Collection;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import com.afpa.dao.ResponsableDao;
import com.afpa.entite.Responsable;

@TestMethodOrder(value = OrderAnnotation.class)
public class ResponsableDaoTest {

	static ResponsableDao dao;
	
	@BeforeAll
	public static void beforAll() {
		dao = new ResponsableDao();
	}
	
	@AfterAll
	public static void afterAll() {
		dao.close();
	}
	
	private static Responsable responsable;
	
	@Test
	@Order(1)
	public void ajoutResponsable() {
		Responsable r = Responsable.builder().id(1).nom("test").build();
		r = dao.add(r);
		assertNotNull(r);
		assertNotEquals(0, r.getId());
		responsable = r;
	}
	
	@Test
	@Order(2)
	public void miseAJourResponsable() {
		responsable.setId(2);
		dao.update(responsable);
		Responsable r = dao.find(responsable.getId());
		assertNotNull(r);
		assertEquals(2, responsable.getId());
	}
	
	@Test
	@Order(3)
	public void listerResponsable() {
		Collection<Responsable> aliments = dao.fndAllNamedQuery("listeResponsable");
		aliments.stream().forEach(System.out::println);
	}
	
	@Test
	@Order(4)
	public void trouverResponsableById() {
		Responsable r = dao.find(responsable.getId());
		assertNotNull(r);
		assertEquals(responsable.getId(), r.getId());
	}
	
	@Test
	@Order(5)
	public void supprimerResponsable() {
		dao.remove(responsable.getId());
		Responsable r = dao.find(responsable.getId());
		assertNull(r);
	}
}
