package com.afpa.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.util.Collection;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import com.afpa.dao.EnclosDao;
import com.afpa.entite.Animal;
import com.afpa.entite.Enclos;

@TestMethodOrder(value = OrderAnnotation.class)
public class EnclosDaoTest {

static EnclosDao dao;
	
	@BeforeAll
	public static void beforAll() {
		dao = new EnclosDao();
	}
	
	@AfterAll
	public static void afterAll() {
		dao.close();
	}
	
	private static Enclos enclos;
	
	@Test
	@Order(1)
	public void ajoutEnclos() {
		Enclos e = Enclos.builder().id(1).nom("test").build();
		e = dao.add(e);
		assertNotNull(e);
		assertNotEquals(0, e.getId());
		enclos = e;
	}
	
	@Test
	@Order(2)
	public void miseAJourEnclos() {
		enclos.setId(2);
		dao.update(enclos);
		Enclos e = dao.find(enclos.getId());
		assertNotNull(e);
		assertEquals(2, enclos.getId());
	}
	
	@Test
	@Order(3)
	public void listerEnclos() {
		Collection<Enclos> aliments = dao.fndAllNamedQuery("listAnimaux");
		aliments.stream().forEach(System.out::println);
	}
	
	@Test
	@Order(4)
	public void trouverEnclosById() {
		Enclos e = dao.find(enclos.getId());
		assertNotNull(e);
		assertEquals(enclos.getId(), e.getId());
	}
	
	@Test
	@Order(5)
	public void supprimerEnclos() {
		dao.remove(enclos.getId());
		Enclos e = dao.find(enclos.getId());
		assertNull(e);
	}
}
