package com.afpa.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.util.Collection;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import com.afpa.dao.AlimentDao;
import com.afpa.entite.Aliment;

@TestMethodOrder(value = OrderAnnotation.class)
public class AlimentDaoTest {

	static AlimentDao dao;
	
	@BeforeAll
	public static void beforAll() {
		dao = new AlimentDao();
	}
	
	@AfterAll
	public static void afterAll() {
		dao.close();
	}
	
	private static Aliment aliment;
	
	@Test
	@Order(1)
	public void ajoutAliment() {
		Aliment a = Aliment.builder().id(1).nom("test").build();
		a = dao.add(a);
		assertNotNull(a);
		assertNotEquals(0, a.getId());
		aliment = a;
	}
	
	@Test
	@Order(2)
	public void miseAJourAliment() {
		aliment.setId(2);
		dao.update(aliment);
		Aliment a = dao.find(aliment.getId());
		assertNotNull(a);
		assertEquals(2, aliment.getId());
	}
	
	@Test
	@Order(3)
	public void listerAliment() {
		Collection<Aliment> aliments = dao.fndAllNamedQuery("listeAliment");
		aliments.stream().forEach(System.out::println);
	}
	
	@Test
	@Order(4)
	public void trouverAlimentById() {
		Aliment a = dao.find(aliment.getId());
		assertNotNull(a);
		assertEquals(aliment.getId(), a.getId());
	}
	
	@Test
	@Order(5)
	public void supprimerAliment() {
		dao.remove(aliment.getId());
		Aliment a = dao.find(aliment.getId());
		assertNull(a);
	}
	
}
