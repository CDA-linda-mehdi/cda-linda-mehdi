package com.afpa.test;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.afpa.dao.AbstractDao;
import com.afpa.entite.Race;

//@TestMethodOrder(value = OrderAnnotation.class)
public class AbstractDaoTest {

	static AbstractDao dao;
	
	@BeforeAll
	public static void beforeAll() {
	dao = new AbstractDao();
	}
	
	@AfterAll
	public static void afterAll() {
	dao.close();
	}
	
}
