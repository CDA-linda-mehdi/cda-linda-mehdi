package com.afpa.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.util.Collection;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import com.afpa.dao.RaceDao;
import com.afpa.entite.Aliment;
import com.afpa.entite.Race;

@TestMethodOrder(value = OrderAnnotation.class)
public class RaceDaoTest {

static RaceDao dao;
	
	@BeforeAll
	public static void beforAll() {
		dao = new RaceDao();
	}
	
	@AfterAll
	public static void afterAll() {
		dao.close();
	}
	
	private static Race race;
	
	@Test
	@Order(1)
	public void ajoutRace() {
		Race r = Race.builder().id(1).nom("test").build();
		r = dao.add(r);
		assertNotNull(r);
		assertNotEquals(0, r.getId());
		race = r;
	}
	
	@Test
	@Order(2)
	public void miseAJourRace() {
		race.setId(2);
		dao.update(race);
		Race r = dao.find(race.getId());
		assertNotNull(r);
		assertEquals(2, race.getId());
	}
	
	@Test
	@Order(3)
	public void listerRace() {
		Collection<Race> aliments = dao.fndAllNamedQuery("listeRace");
		aliments.stream().forEach(System.out::println);
	}
	
	@Test
	@Order(4)
	public void trouverRaceById() {
		Race r = dao.find(race.getId());
		assertNotNull(r);
		assertEquals(race.getId(), r.getId());
	}
	
	@Test
	@Order(5)
	public void supprimerRace() {
		dao.remove(race.getId());
		Race r = dao.find(race.getId());
		assertNull(r);
	}
}
