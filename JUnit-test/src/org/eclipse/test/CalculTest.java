package org.eclipse.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.RepetitionInfo;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import com.afpa.main.Calcul;

@DisplayName("Test de la classe Calcul")
class CalculTest {
	Calcul calcul;
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
		calcul = new Calcul();
	}

	@AfterEach
	void tearDown() throws Exception {
		calcul = null;
	}

	//@Disabled
	@Test
	void testSomme() {
		if (calcul.somme(2, 3) != 5)
			fail("faux pour deux entiers positifs");
		if (calcul.somme(-2, -3) != -5)
			fail("faux pour deux entiers n�gatifs");
		if (calcul.somme(-2, 3) != 1)
			fail("faux pour deux entiers de signe diff�rents");
		if (calcul.somme(0, 3) != 3)
			fail("faux pour x nul");
		if (calcul.somme(2, 0) != 2)
			fail("faux pour y nul");
		if (calcul.somme(0, 0) != 0)
			fail("faux pour x et y nuls");
	}

	//@Disabled
	@Test
	void testDivision() {
		assertFalse("2entiers positifs", calcul.division(6, 3) == 0);
		assertEquals("2entiers n�gatifs", 2, calcul.division(-6, -3));
		assertNotNull("2 entiers de signes diff�rents", calcul.division(-6, 3));
		assertTrue("entiers x nul", calcul.division(0, 3) == 0);
		Throwable e = null;
		try {
			calcul.division(2, 0);
		}
		catch (Throwable ex) {
			e = ex;
		}
		assertTrue(e instanceof ArithmeticException);
		e = null;
		try {
			calcul.division(0, 0);
		}
		catch (Throwable ex) {
			e =ex;
		}
		assertTrue (e instanceof ArithmeticException);
	}
	
	@RepeatedTest(3)
	void testSomme (RepetitionInfo repetitionInfo) {
		assertNotEquals (7, calcul.somme(repetitionInfo.getCurrentRepetition(), 3));
	}

	@DisplayName("Test de la m�thode somme param�tr�e")
	@ParameterizedTest
	@ValueSource (ints = {2,3})
	void testSomme(int t) {
		assertNotEquals(5, calcul.somme(t, 2));
	}
}
 