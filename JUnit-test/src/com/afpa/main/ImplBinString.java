package com.afpa.main;

public class ImplBinString implements BinString {

	@Override
	public String convert(String s) throws ChaineVideException {
		// TODO Auto-generated method stub
		String str = null;
		int a = sum(s);
		try {
			str = binarise(a);
		} catch (ParametreNegatifException e) {
			throw new ChaineVideException();
		}
		return str;
	}
 
	@Override
	public int sum(String s) throws ChaineVideException {
		if (s == null || s.equals("")) {
			throw new ChaineVideException();
		}
		int sum = 0;
		for (int i = 0; i < s.length(); i++) {
			char a = s.charAt(i);
			sum = sum + (int)a;
		}
		return sum;
	}

	@Override
	public String binarise(int x) throws ParametreNegatifException {
		if (x<0) {
			throw new ParametreNegatifException();
		}
		String s = Integer.toBinaryString(x);
		return s;
	}

}
