package com.afpa.main;

public interface BinString {
	
	
	
	
	 // transforme une chaine de caract�re en la repr�sentation binaire
	 // de la somme du code ascii des caract�res.
	 // si param�tre chaine vide ou param�tre null l�ve une ChaineVideException
	 public String convert(String s) throws ChaineVideException;
	 
	 
	 
	 // retourne la somme du code ascii des caract�res du param�tre
	 // si param�tre chaine vide ou param�tre null l�ve une ChaineVideException
	 public int sum(String s) throws ChaineVideException;
	 
	 
	 // retourne la repr�sentation binaire du param�tre
	 // sans padding � gauche avec des z�ros
	 // si param�tre n�gatif l�ve une ParametreNegatifException
	 public String binarise(int x) throws ParametreNegatifException;

}
