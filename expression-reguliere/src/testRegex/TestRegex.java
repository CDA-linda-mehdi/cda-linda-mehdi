package testRegex;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TestRegex {

	private static Pattern pattern;
	private static Matcher matcher;
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("******************EXEMPLE**********************");
		pattern = Pattern.compile("Hugo");
		matcher = pattern.matcher("Hugo Eti�vant");
		while (matcher.find()) {
			System.out.println("Trouv� !");
		}
		
		System.out.println("******************EXEMPLE AVEC CHAINE LITERALES**********************");
		pattern = Pattern.compile("simple cha�ne de caract�res");
		matcher = pattern.matcher("autre simple cha�ne de caract�res");
		while (matcher.find()) {
			System.out.println("Trouv� aussi !");
		}
		
		System.out.println("\n******************LISTE DES  METACARACTERES**********************");
		Map<String, String> listeMetacaracteres = new LinkedHashMap<>();
		listeMetacaracteres.put(". ", "Remplace tout caract�re");
		listeMetacaracteres.put("* ", "Remplace une cha�ne de 0, 1 ou plusieurs caract�res");
		listeMetacaracteres.put("? ", "Remplace exactement un caract�re");
		listeMetacaracteres.put("[]", "Intervalle de caract�res");
		listeMetacaracteres.put("{}", "Quantificateur");
		listeMetacaracteres.put("\\ ", "D�sp�cialise le caract�re sp�cial qu'il pr�c�de");
		listeMetacaracteres.put("^ ", "N�gation en d�but de ligne");
		listeMetacaracteres.put("� ", "Fin de ligne");
		listeMetacaracteres.put("| ", "Ou logique entre deux sous-motifs");
		listeMetacaracteres.put("+ ", "Num�rateur");
		for (String e : listeMetacaracteres.keySet()) {
			System.out.println(e + " : " + listeMetacaracteres.get(e));
		}
		
		System.out.println("\n******************CLASSES DE CARACTERES**********************");
		Map<String, String> listeClasseDeCaracteres = new LinkedHashMap<>();
		listeClasseDeCaracteres.put("\\d ", "Un chiffre �quivalent � : [0-9]");
		listeClasseDeCaracteres.put("\\D ", "Un non-chiffre : [^[0-9]");
		listeClasseDeCaracteres.put("\\s ", "Un caract�re blanc : [ \\t\\n\\x0B\\f\\r]");
		listeClasseDeCaracteres.put("\\S ", "Un non-caract�re blanc : [^\\s]");
		listeClasseDeCaracteres.put("\\w ", "Un caract�re de mot : [a-zA-Z_0-9]");
		listeClasseDeCaracteres.put("\\W ", "Un caract�re de non-mot : [^\\W");
		listeClasseDeCaracteres.put(". ", "Tout caract�re");
		for (String f : listeClasseDeCaracteres.keySet()) {
			System.out.println(f + " : " + listeClasseDeCaracteres.get(f));
		}
		System.out.println("\nR�gles de constructions des classes personnalis�es : ");
		Map<String, String> listeClassePersonnalisees = new LinkedHashMap<>();
		listeClassePersonnalisees.put("[abc]               ", "Ensemble simple, remplace tout caract�re parmi les caract�res suivants : a, b et c");
		listeClassePersonnalisees.put("[^abc]              ", "N�gation de l'ensemble pr�c�dent");
		listeClassePersonnalisees.put("[a-z]               ", "Ensemble complexe, remplace tout caract�re parmi ceux de l'alphabet naturel compris entre a et z");
		listeClassePersonnalisees.put("[a-zA-Z] [a-z[A-Z]] ", "Union d'ensemble, remplace tout caract�re de l'alphabet minuscule ou majuscule");
		listeClassePersonnalisees.put("[abc&&[a-z]         ", "Intersection d'ensembles, remplace tout caract�re faisant partie de l'ensemble : a, b, c et aussi de l'ensemble de a jusqu'� z (c'est � dire uniquement a, b et c)");
		listeClassePersonnalisees.put("[a-a&&[^abc]]       ", "Soustraction d'ensembles, remplace tout caract�re de l'alphabet compris entre a et z, except� ceux de l'intervalle suivant : a, b et c");
		for (String f : listeClassePersonnalisees.keySet()) {
			System.out.println(f + " : " + listeClassePersonnalisees.get(f));
		}
		
		System.out.println("\n******************QUANTIFICATEURS**********************");
		Map<String, String> listeDesQuantificateurs = new LinkedHashMap<>();
		listeDesQuantificateurs.put("X?      ", "Une fois ou pas du tout");
		listeDesQuantificateurs.put("X*      ", "Z�ro une fois ou plusieurs fois");
		listeDesQuantificateurs.put("X+      ", "Une fois ou plusieurs fois");
		listeDesQuantificateurs.put("X{n}    ", "Exactement n fois");
		listeDesQuantificateurs.put("X{n, }  ", "Au moins n fois");
		listeDesQuantificateurs.put("X{n, m} ", "Au moins n fois et jusqu'� m fois");
		for (String g : listeDesQuantificateurs.keySet()) {
			System.out.println(g + " : " + listeDesQuantificateurs.get(g));
		}
		
		System.out.println("\n\n****************** TESTS : **********************");
		System.out.println("\nTest avec les m�tacaract�res (ou symboles) : ");
		
		System.out.println("\nsymbole .");
		System.out.println("Entrez votre chaine : ");
		String str = sc.nextLine();
		pattern = Pattern.compile("tot.");
		matcher = pattern.matcher(str);
		while (matcher.find()) {
			System.out.println(matcher.group());
		}
		
		System.out.println("\nsymbole *");
		System.out.println("Entrez votre chaine : ");
		String str1 = sc.nextLine();
		pattern = Pattern.compile("tot*{2}");
		matcher = pattern.matcher(str1);
		while (matcher.find()) {
			System.out.println(matcher.group());
		}
		
		System.out.println("\nsymbole ?");
		System.out.println("Entrez votre chaine : ");
		String str2 = sc.nextLine();
		pattern = Pattern.compile("tot?");
		matcher = pattern.matcher(str2);
		while (matcher.find()) {
			System.out.println(matcher.group());
		}
 	}

}
