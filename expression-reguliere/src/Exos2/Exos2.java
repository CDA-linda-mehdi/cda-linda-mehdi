package Exos2;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Exos2 {

	private static Pattern pattern;
	private static Matcher matcher;
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Lecture des cha�nes de caract�res comportant des chiffres et comman�ant et terminant par des non-chiffres.");
		System.out.println("Veuillez entrer votre cha�ne de caract�res : ");
		String str = sc.nextLine();
		pattern = pattern.compile("\\D\\d+\\D");
		matcher = pattern.matcher(str);
		
		while (matcher.find()) {
			System.out.print(matcher.group());
		}
	}
}
