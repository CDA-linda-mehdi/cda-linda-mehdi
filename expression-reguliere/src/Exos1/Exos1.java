package Exos1;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Exos1 {

	private static Pattern pattern;
	private static Matcher matcher;
	
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Lecture des cha�nes de caract�res qui correspondent � des nombres :");
		System.out.println("Veuillez entrer votre cha�ne de caract�res : ");
		String str = sc.nextLine();
		pattern = Pattern.compile("[0-9]");
		matcher = pattern.matcher(str);
		while (matcher.find()) {
			System.out.print(matcher.group());
		}
	}

}
