package com.afpa.jacoco_test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class TestMessageBuild {

	@Test
	public void testNameMkyong() {
		MessageBuild obj = new MessageBuild();
		assertEquals("Hello mkyong", obj.getMessage("mkyong"));
	}
	
	@Test
	public void testNameEmpty() {
		MessageBuild obj = new MessageBuild();
		assertEquals("Please provide a name !", obj.getMessage(" "));
	}

	@Test
	public void testNameNull() {
		MessageBuild obj = new MessageBuild();
		assertEquals("Please provide a name !", obj.getMessage(null));
	}
}