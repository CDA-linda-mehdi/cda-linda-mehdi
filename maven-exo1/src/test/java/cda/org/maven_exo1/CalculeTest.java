package cda.org.maven_exo1;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CalculeTest {

	@BeforeAll
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	public static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	public void setUp() throws Exception {
	}

	@AfterEach
	public void tearDown() throws Exception {
	}

	
	
	@Test
	public void testAddition() {
		if (Calcule.addition(2, 3) != 5)
			fail("faux pour deux entiers positifs");
		if (Calcule.addition(-2, -3) != -5)
			fail("faux pour deux entiers négatifs");
		if (Calcule.addition(-2, 3) != 1)
			fail("faux po_ur deux entiers de signe différents");
		if (Calcule.addition(0, 3) != 3)
			fail("faux pour x nul");
		if (Calcule.addition(2, 0) != 2)
			fail("faux pour y nul");
		if (Calcule.addition(0, 0) != 0)
			fail("faux pour x et y nuls");
	}

	@Test
	public void testDivision() {
		assertFalse(Calcule.division(6, 3) == 0, "2entiers positifs");
		assertEquals(2, Calcule.division(-6, -3), "2entiers négatifs");
		assertNotNull(Calcule.division(-6, 3), "2 entiers de signes différents");
		assertTrue(Calcule.division(0, 3) == 0, "entiers x nul");
		Throwable e = null;
		try {
			Calcule.division(2, 0);
		}
		catch (Throwable ex) {
			e = ex;
		}
		assertTrue(e instanceof ArithmeticException);
		e = null;
		try {
			Calcule.division(0, 0);
		}
		catch (Throwable ex) {
			e =ex;
		}
		assertTrue (e instanceof ArithmeticException);
	}

}
