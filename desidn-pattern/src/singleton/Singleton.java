package singleton;

//Seulement un seul objet de cette calsse peut �tre cr�e
public class Singleton {

	//Instance unique non pr�initialis�e
	private static Singleton instance = null;
	
	//Un constructeur priv� qui emp�che toute autre classe de l'instancier
	private Singleton() {
	}
	
	//M�thode qui fournit un moyen d'instancier la classe et d'en retourner une instance
	public static Singleton getInstance() {
		if (instance == null)
			instance =new Singleton();
		return instance;
	}
}
