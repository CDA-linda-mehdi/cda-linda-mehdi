package calc;

import java.util.Arrays;
import java.util.Optional;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import javax.xml.transform.stream.StreamSource;

public class CalculatriceTableauStream {

	public static int sommeElements(int tab[]) {

		return Arrays.stream(tab).sum();

	}

	public static int plusPetitElement(int[] a) {
		//return Arrays.stream(a).min().getAsInt();
		int res = IntStream.of(a).map(se->se).sum();
		return res;
	}

	public static int plusGrandElement(int tab2[]) {
		return Arrays.stream(tab2).max().getAsInt();
	}

	public static long sommeElementsDeuxTableaux(int[] tabInt, int[] tabInt2) {
		int res1 = CalculatriceTableauStream.sommeElements(tabInt);
		int res2 = CalculatriceTableauStream.sommeElements(tabInt2);
		long total = res1 + res2;
		return total;
	}

	public static int[] triAscendant(int[] tabInt) {
		return Arrays.stream(tabInt).sorted().toArray();
	}

	// public static boolean conjonction(boolean tab6[]) {
	public static Optional<Boolean> conjonction(boolean tab[]) {
		Boolean[] test = new Boolean[tab.length];
		for (int i = 0; i < test.length; i++) {
			test[i] = tab[i];
		}
		boolean a = Arrays.stream(test).anyMatch(x -> x == false);
		if (a) {
			return Optional.of(a);
		} else {
			return Optional.empty();
		}
	}

	public static long nombreDElementsPair(int tab7[]) {
		long res = Arrays.stream(tab7).filter(x->x%2 == 0).count();
		
		return res;
	}

	// 2�me m�thode avec une expression ternaire
	public static long nombreDElementsPairBis(int tab8[]) {
		int pair = 0;
		for (int i = 0; i < tab8.length; i++) {
			pair = (tab8[i] % 2 == 0) ? pair + 1 : pair; // <= expression ternaire, voir commentaire juste en-dessous
		}
		return pair;
	}
	/*
	 * l'expression ternaire ci-dessus vaut : if(tab8[i] % 2 = 0) { pair = pair + 1;
	 * } else { pair = pair }
	 */

	public static boolean chercheSiUnElementExiste(int param, int tab9[]) {
		
		boolean e = Arrays.stream(tab9).anyMatch(x->x == param);
				//.filter(x->x ==param).findAny().isPresent();
		return e;
	}

	public static int[] mettreZeroDansLesCasesAIndicesImpair(int tab[]) {
		Stream.iterate(0,  i-> i +1).limit(tab.length).filter(x->x%2 == 1).forEach(x-> {
			tab[x] = 0;
		});
		for (int i = 0; i < tab.length; i++) {
			if (i % 2 == 1) {
				tab[i] = 0;
			}
		}
		return tab;
	}

	/*
	 * On peut aussi utiliser cette technique Ne sourtout pas confondre i qui
	 * contient la position de la case et [i] qui cotient la valeur de la case
	 * public static int[] mettreZeroDansLesCasesAIndicesImpair(int[] tab) { for
	 * (int i =1; i<tab.length; i+=2) { tab[i] = 0;//rends les valeurs impaires = 0
	 * } return tab; }
	 */

	public static int[] decalerLesElementsTableauDUneCase(int tab1[]) {
		int tab2[];
		tab2 = new int[tab1.length];
		for (int i = 0; i < tab1.length - 1; i++) {
			tab2[i + 1] = tab1[i];
		}
		tab2[0] = tab1[tab1.length - 1];
		return tab2;
	}

	public static int[] triAscendantDeuxTableaux(int[] a, int[] b) {		
		int e = 0;
		int[] d = new int[a.length + b.length];
		boolean c;

		for (int i = 0; i < a.length; i++) {
			d[i] = a[i];
		}

		for (int i = 0; i < b.length; i++) {
			d[i + a.length] = b[i];
		}

		do {
			c = false;

			for (int i = 0; i < d.length - 1; i++) {
				if (d[i] > d[i + 1]) {
					e = d[i];
					d[i] = d[i + 1]; // Echangeur
					d[i + 1] = e; //
					c = true;
				}
			}
		} while (c);

		return d;
	}

	// autre technique
	/*
	 * public static int[] triAscendantDeuxTableaux(int[] tab1, int[] tab2) {
	 * 
	 * int[] tabf; tabf = new int[tab1.length + tab2.length];
	 * 
	 * for(int i=0; i<tab1.length;i++) { tabf[i] = tab1[i]; }
	 * 
	 * for(int i=0; i<tab2.length;i++) { tabf[i+tab1.length] = tab2[i]; }
	 * 
	 * return triAscendant(tabf);
	 * 
	 * }
	 */
}
