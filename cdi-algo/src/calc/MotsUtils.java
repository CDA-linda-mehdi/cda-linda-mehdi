package calc;

import java.util.function.Function;
import java.util.stream.Collectors;

public class MotsUtils {

	public static String inverser(String str) {
		String a = "";

		for (int i = str.length() -1; i>= 0; i--) {
			a = a + str.charAt(i);

		}
		return a;

	}

	public static String caracteresCommuns(String str, String str2) {
		String res = "";

		for (int i = 0; i < str.length(); i++) {

			char leCaractereEnCours = str.charAt(i);

			for(int j = 0; j<str2.length(); j++) {

				if (leCaractereEnCours == str2.charAt(j)) {

					int indexCaractereEnCourDansRes = res.indexOf(leCaractereEnCours);

					if(indexCaractereEnCourDansRes == -1) {
						res = res + leCaractereEnCours;
					}

				}

			}

		}
		return res;
	}

	// autre m�thode :
	/*
	public static String caracteresCommuns(String str, String str2) {

        String strf ="";
        boolean bool = true;

        for(int i =0; i<str.length(); i++) {
            bool = true;
            for(int j =0; bool && j<strf.length(); j++) {
                if(str.charAt(i) != strf.charAt(j)) {
                    bool = true;
                }
                else {
                    bool = false;
                }
            }
            if(bool){
                for(int k =0; bool && k<str2.length(); k++) {
                    if(str.charAt(i)==str2.charAt(k)) {
                        strf = strf + str.charAt(i);
                        bool = false;
                    }
                }
            }
        }


        return strf;
    }
	 */

	/*
	public static Boolean estUnPalindrome(String str) {
		String str = "";
		for (int i = 0; i < str.length(); i++) {
			char leCaractereEnCoursDebut = str.charAt(i);
			for (int j = str.length() -1; j < str.length(); j--) {
				char leCaractereEnCoursFin = str.charAt(j);
				if (leCaractereEnCoursDebut == leCaractereEnCoursFin) {
					return true;
				} else {
					return false;
				}
			}
		}
	}
	 */

	// autres m�thodes :

	public static boolean estUnPalindrome(String a) {
		return a.equals(MotsUtils.inverser(a));
	}


	/*
	   public static boolean estUnPalindrome(String str) {

        String strInverse ="";
        boolean bool = true;

        for(int j=0; bool && j<(str.length()/2);j++) {
            if(str.charAt(j) != str.charAt(str.length() - j -1)) {
                bool = false;
            }
        }

        return bool;
    }
	 */


	/*
	   public static boolean estUnPalindrome (String str) {


        int z=str.length()-1;
        boolean res=true;

        for (int a=0;a<z&&res;a++) {
            if (str.charAt(a) != str.charAt(z)) {
                res = false;
            }
            z--;
        }
        return res;
    }
}
	 */

	/*
	public static long sommeChiffresDansMot(String str) {
		String str = "";
		long res;
		for (int i = 0; i < str.length(); i++) {
			char leCaractereEnCours = str.charAt(i);
			if (leCaractereEnCours.isDigit(i) )

		}
		return 0;

	}
	 */


	/* M�thode de Redouane */
	/*
	 public static long sommeChiffresDansMot(String str) {

        long somme = 0;

        for(int i=0; i<str.length(); i++) {

            if (47<str.charAt(i) && str.charAt(i)<58){
            somme = somme + (int)str.charAt(i) - 48;
            }
        }

        return somme;
    }
	 */


	/* M�thode de Maxime modifi�*/

	public static long sommeChiffresDansMot(String str) {


		String str2 = "";

		for (int i = 0; i < str.length(); i++) {

			if (str.charAt(i) == '1' || str.charAt(i) == '2' || str.charAt(i) == '3' || str.charAt(i) == '4'
					|| str.charAt(i) == '5' || str.charAt(i) == '6' || str.charAt(i) == '7' || str.charAt(i) == '8'
					|| str.charAt(i) == '9') {
				str2 = str2 + str.charAt(i);

			}
		}

		System.out.println("str2 = "+ str2);

		int[] chiffres = new int[str2.length()];

		int grosChiffre = Integer.parseInt(str2);
		System.out.println("grosChiffre : "+grosChiffre);

		for (int i = 0; i < chiffres.length; i++) {
			chiffres[i] = grosChiffre % 10;
			grosChiffre = grosChiffre / 10;
			System.out.println(chiffres[i]);
		}

		long somme = 0;
		for (int i = 0; i < chiffres.length; i++) {
			somme = somme + chiffres[i];
		}

		return somme;
	}





	/* M�thode de Yassine */
	/*
	public static long sommeChiffresDansMot(String mystr) {

		String mystr2 = mystr.replaceAll( "[^\\d]", "" );
		System.out.println(mystr2);
		long numero= Integer.parseInt(mystr2);

		long somme = 0;

		while ( numero != 0) {
			somme += numero % 10;
			numero = numero /10;
		}
		return somme;


	}
	 */

	/* M�thode de Cl�ment */
	/*
	public static long sommeChiffresDansMot(String str) {

		long sum=0;
		for(int i=0; i<str.length(); i++){
			if(Character.isDigit(str.charAt(i))){
				sum = sum+str.charAt(i)-48;
			}
		}
		return sum;
	}
	 */

	/* M�thode de Laurent */
	/*
	public static long sommeChiffresDansMot (String str) {

		long som = 0;
		int a=0;
		for (int i=0;i<str.length();i++) {
			char monchar = str.charAt(i);
			if (Character.isDigit(monchar)) {
				a=Character.getNumericValue(monchar);
				som=som+a;
			}
		}
		return som;    
	}
	 */

	public static void afficherNombreOccurence(String str) {
		str.toLowerCase()
		.chars().boxed()
		.collect(Collectors.groupingBy( Function.identity(), Collectors.counting()))
		.forEach((k,v)->{System.out.println(((char)k.intValue()) + " --- " + v);});
	}

	/* Ma m�thode */
	/*
	public static String devinerAlgo(int param) {
		String res;
		int multi = 0;
		int sous = 0;
		int total;
		multi = param * 2;
		sous = param - 1;
		total = multi + sous;
		res = total;

		return res;
	}
	 */


	/* M�thode de Cl�ment */

	public static String devinerAlgo(int param) {
		int cal = param * 2;
		int cal1 = param -1;
		String str = String.valueOf(cal);
		String str1 = String.valueOf(cal1);
		String res = str+str1;
		return res;
	}




	/* M�thode de Redouane */
	/*
	public static String devinerAlgo(int param) {
        return "" + 2*param + (param-1);
    }
	 */


	/* M�thode Mustapha */
	/*
	public static void afficherNombreOccurence(String str) {
		String lettresDejaVues = "";

		for(int i = 0; i<str.length();i++) {
			char laLettreEnCours = str.charAt(i);// lalettreEnCours = lettre que je traite
			if(lettresDejaVues.indexOf(laLettreEnCours) == -1) {
				int compteur = 0;// reinitialise le compteur pour le i suivant cad lettre suivante

				for(int j =0; j<str.length();j++) {
					if(laLettreEnCours==str.charAt(j)) {// si la lettre que je traite = lettre que je compare
						compteur = compteur + 1;// j'incremente le compteur
					}        
				}

				System.out.println(laLettreEnCours+" --- " + compteur);
				lettresDejaVues=lettresDejaVues + laLettreEnCours;
				System.out.println("lettresDejaVues : "+lettresDejaVues);
			}
		}
	}
	 */

	/* M�thode Jean-Philippe */ 
	/*
	public static void afficherNombreOccurence1(String a) {
		int d = 0;
		char[] c = a.toCharArray();
		for (int i = 0; i < a.length(); i++) {
			if(a.charAt(i)==' ') {
				continue;
			}
			d = 0;
			for (int j = 0; j < a.length(); j++) {
				if (a.charAt(i) == a.charAt(j)) {
					d++;
				}
			}

			System.out.println(a.charAt(i) + " : " + d);
			a = a.replaceAll(Character.toString(c[i]), " ");
		}
	}
	 */


	// Ma m�thode
	/*
	public static String exercice29(int param, int param2) {
		int tab[];
		tab = new int[param2];
		if (param % 2 == 0) {
			param = param + 1;
		} else {
			param = param + 2;

		}

		System.out.println("param : "+param);

		for (int i = 0; i < tab.length; i++) {
			System.out.println("avant le i : "+i);
			tab[i] = param;
			param=param+2;
		}
		System.out.println("********************");
		for (int i = 0; i < tab.length; i++) {
			System.out.print(tab[i]+"*");
		}

		System.out.println();

//		String str = tab;
		return null;
	}
	 */

	// M�thode Amaia
	/*
	public static String exercice29(int param, int param2) {
        String s = "";
        if (param % 2 == 0) {
            param = param + 1;
        }
        for (int cpt = 0; cpt < param2; cpt++) {
            param = param + 2;
            s = s + " " + param;
        }
        return s;

	}
	 */

	//M�thode Laurent
	public static String exercice29 (int num,int nbre) {

		String res="";
		if (num%2==0) {
			num=num+1;
		}
		else {
			num=num+2;
		}
		res=res+num;

		for (int i=1;i<nbre;i++) {
			num=num+2;
			res=res+"*"+num;
		}
		return res;
	}

	// Ma M�thode
	/*
	public static long sommeUnicodes(String str) {
		long res = 0;
		System.out.println(str + "*********************************");
		for (int i = 0; i < str.length(); i++) {
		int caratereEnCours = str.charAt(i);
		System.out.println(caratereEnCours);
		res = res + caratereEnCours;
		}
		return res;
	}
	 */

	// M�thode Anthony
	/*
	public static long sommeUnicodes(String str) {
        long a = 0;
        for(int i=0;i<str.length();i++) {
            a =  a + str.charAt(i);
        }

        return a;

    }
	 */

	// M�thode Souliman

	public static long sommeUnicodes(String str )  {

		long res = 0l;
		for (int i=0;i<str.length();i++) {
			res = res + str.charAt(i);
		}
		return res;
	}

	// Ma m�thode
	/*
	public static int convertirBinaireEnDecimal(String str) {
		int res = 0;
		for (int i = 0; i < str.length(); i++) {
			res = res + str.charAt(i);
		}
		System.out.println("*****************************************");
		System.out.println(res);
		return res;

	}
	 */

	// M�thode Cl�ment
	public static int convertirBinaireEnDecimal(String str) {
		long sum=0;
		int res =0;
		for(int i=0; i<str.length(); i++){
			System.out.println("i "+ i);

			sum = str.charAt(i)-48;
			System.out.println("  sum "+ sum);
			res +=  (int) (sum*Math.pow(2,str.length()-i-1));

			System.out.println("  res "+ res);
		}
		return res;
	}

	// Ma m�thode
	/*
	public static long donnerLaDecimaleDeLaLettre(String str) {
		long res = 0;
		String str = new string[26];
		for (int i = 0; i < str.length(); i++) {

		}
		return 0;
	}
	 */

	// M�thode Yassine
	/*
	public static String donnerLaDecimaleDeLaLettre(String str) {

		String res  = " ";



		String str2 = str.toLowerCase();

		for (int i = 0 ; i< str.length(); i++) {



				res += str2.charAt(i) -96;
			}



		return res;


	}
	 */

	// M�thode Mustapha
	/*
	public static String donnerLaDecimaleDeLaLettre(String str) {


		String param1 = str.toUpperCase();

		String str1 = " ";


		for (int i=0; i<param1.length(); i++ ) {



			str =  str + (param1.charAt(i) - 64);
		}


		return str;

	}
	 */

	// M�thode Anthony

	public static String donnerLaDecimaleDeLaLettre(String str) {
		String res = "";
		String tabmaster = "abcdefghijklmnopqrstuvwxyz";
		String str2 = str.toLowerCase();
		System.out.println(""+str2);
		for(int i = 0;i<str2.length();i++) {
			char c = str2.charAt(i);
			int indiceDeLaLettre = tabmaster.indexOf(c);
			res = res + (indiceDeLaLettre+1);

		}
		return res;
	}

	// Ma m�thode
	/*
	public static String entreeAlphanumerique(String str) {

		return null;
	}
	 */


	// M�thode Maxime
	/*
	public static String entreeAlphanumerique(String str) {

		String tempo = "";
		String res = "";
		int multi = 0;

		for (int i = 0; i < str.length(); i++) {

			if (Character.isDigit(str.charAt(i))) {
				tempo = tempo + str.charAt(i);

			} else if (str.charAt(i) != str.charAt(0) && !Character.isDigit(str.charAt(i))
					&& Character.isDigit(str.charAt(i - 1))) {
				multi = Integer.parseInt(tempo);
				for (int j = 0; j < multi; j++) {
					res = res + str.charAt(i);
				}
				tempo = "";

			} else {

				res = res + str.charAt(i);
			}

		}

		return res;
	}
	 */

	// M�thode Laurent
	/*
	public static String entreeAlphanumerique (String str) {

		String res="";	
		String nbre="";
		str+=" ";  // Ajout espace sinon erreur avec 1 chiffre en dernier
		int chif=1;

		for (int i=0;i<str.length();i++) {

			if (Character.isDigit(str.charAt(i))) { 

				if (Character.isDigit(str.charAt(i+1))) {  
					nbre += str.charAt(i);
				}
				else {
					nbre += str.charAt(i); 
					chif=Integer.parseInt(nbre);
					i++;
				}
			}
			else { }

			for (int j=0;j<chif&&!Character.isDigit(str.charAt(i));j++) {
				res += str.charAt(i);
				nbre="";
			}
			chif=1;
		}
		return res;
	}
	 */

	// M�thode Redouane
	/*
	public static String entreeAlphanumerique(String str) {
		String res = "";
        String multipleStr = "";
        int multipleInt =0;


        for(int i=0; i<str.length(); i++) {
            if( (47 < str.charAt(i) && str.charAt(i) < 58)) {
                multipleStr = multipleStr + str.charAt(i);
            }
            else {

                for(int k=0; k<multipleStr.length();k++) {
                    multipleInt = multipleInt + (multipleStr.charAt(k)-48)*(int)Math.pow(10,multipleStr.length()-1-k);
                }

                for(int j=0; j<=multipleInt; j++) {

                    res = res + str.charAt(i);
                }
                multipleStr = "";
                multipleInt = 0;    

            }
        }

        return res;
    }
	 */

	// M�thode Isame

	public static void entreeAlphanumerique(String str) {
		System.out.print("la transformation de "+str+" : ");
		String nombreDeFois = "";
		for(int i = 0; i< str.length(); i++) {

			// je recupere le caractere en cours
			char leCaractereEnCours = str.charAt(i);

			if( !Character.isDigit(leCaractereEnCours) && nombreDeFois.length() == 0) { 
				// si le caractere n'est pas un digit et je n'ai pas accumul� "nombre de fois" j'affiche le caractere 
				System.out.print(leCaractereEnCours);

			} else if(Character.isDigit(leCaractereEnCours)) {
				// si le caractere en cours est un digit, je l'accumule dans "nombre de fois"
				nombreDeFois+=leCaractereEnCours;

			} else {
				// si le caractere n'est pas un digit et j'ai accumul� des chiffres dans "nombreDeFois"

				// je transforme nombreDeFois en une valeur numerique
				int nombreDeFoisNumerique = Integer.parseInt(nombreDeFois);

				// j'affiche autant de "nombreDeFois" le caractere en cours
				for(int j=0; j<nombreDeFoisNumerique; j++) {
					System.out.print(leCaractereEnCours);
				}
				nombreDeFois="";
			}
		}
		System.out.println();
	}

	// Me m�thode

	public static int resultatPositifNegatif(String str) {
		int res = 0;
		int transforme = 0;
		String recupNbre = "";
		char leCaractereEnCours = 'a';
		

		for (int i = 0; i < str.length(); i++) {

			// Je r�cup�re le caract�re en cours
			leCaractereEnCours = str.charAt(i);

			if (!Character.isDigit(leCaractereEnCours)) {
				System.out.println(leCaractereEnCours);

				switch(leCaractereEnCours) {
				case '-':
					System.out.println("-");
					break;
				case '+':
					System.out.println("+");
					break;
				default:
					System.out.println("N'est pas le caract�re attendu");
				}


			} else {
				recupNbre = recupNbre + leCaractereEnCours;
				System.out.println(recupNbre);
			}



			
		}

		// Je transforme la variable recupNbre en variable de type entier
		transforme = Integer.parseInt(recupNbre);
		System.out.println(recupNbre);




		return res;
	}

	


}
