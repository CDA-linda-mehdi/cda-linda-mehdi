package calc;

public class CalculatriceTableau {




	public static int sommeElements(int tab[]) {

		int resultat = 0;

		for (int i = 0; i < tab.length; i++) {
			resultat = resultat + tab[i];
		}
		return resultat;

	}


	public static int plusPetitElement(int tab1[]) {
		int resultat = 0;
		int min = Integer.MAX_VALUE;

		for (int i = 0; i < tab1.length; i++) {
			resultat = tab1[i];
			if (min > resultat) {
				min = resultat;
			}
		}
		return min;
	}


	public static int plusGrandElement(int tab2[]) {
		int resultat = 0;
		int max = Integer.MIN_VALUE;

		for (int i = 0; i < tab2.length; i++) {
			resultat = tab2[i];
			if (max < resultat) {
				max = resultat;
			}
		}
		return max;
	}



	public static int sommeElementsDeuxTableaux (int[] tabInt, int[]tabInt2) {
		int som1 = 0;
		for (int i = 0 ; i  < tabInt.length ; i++) {
			som1 = som1 + tabInt [i];
		}
		int som2=0;
		for (int i = 0 ; i  < tabInt2.length ; i++) {
			som2 = som2 + tabInt [i];
		}

		int som = som1 + som2;		
		return som;
	}

	public static int[] triAscendant(int[] tabInt) {
		//        java.util.Arrays.sort(tabInt);
		int b=0;
		int tab1[]=new int[tabInt.length];
		tab1=tabInt.clone();
		for(int i=0;i<tab1.length-1;i++)
		{
			for(int j=i+1;j<tab1.length;j++)
			{
				if(tab1[i]>tab1[j])
				{
					b=tab1[i];
					tab1[i]=tab1[j];
					tab1[j]=b;
				}
			}
		}

		return tab1;
	}

	public static boolean conjonction(boolean tab6[]) {
		boolean f = true;
		for (int i = 0; i < tab6.length; i++) {
			f = f && tab6[i];
		}
		return f;
	}


	// 2�me m�thode :
	public static boolean conjonction1(boolean tab6[]) {
		boolean f = true;
		for (int i = 0; f && i < tab6.length; i++) {
			f = f && tab6[i];
		}
		return f;
	}

	public static long nombreDElementsPair(int tab7[]) {
		long a = 0;
		for (int i = 0; i < tab7.length; i++) {
			if (tab7[i] % 2 == 0) {
				a++;
			}
		}
		return a;
	}				


	// 2�me m�thode avec une expression ternaire
	public static long nombreDElementsPairBis(int tab8[]) {
		int pair = 0;
		for (int i = 0; i < tab8.length; i++) {
			pair = (tab8[i] % 2 == 0) ? pair + 1 : pair; //<= expression ternaire, voir commentaire juste en-dessous
		}
		return pair;
	}
	/* l'expression ternaire ci-dessus vaut :
	if(tab8[i] % 2 = 0) {
		pair = pair + 1;
	} else {
		pair = pair
	}
	 */


	public static boolean chercheSiUnElementExiste(int param, int tab9[]) {
		boolean e = false;
		for (int i = 0; i < tab9.length; i++) {
			if (tab9[i] == param) {
				e = true;
			}
		}
		return e;
	}

	public static int[] mettreZeroDansLesCasesAIndicesImpair(int tab10[]) {
		for (int i = 0; i < tab10.length; i++) {
			if (i % 2 == 1) {
				tab10[i] = 0;
			}
		}
		return tab10;
	}

	/* On peut aussi utiliser cette technique
	 * Ne sourtout pas confondre i qui contient la position de la case et [i] qui cotient la valeur de la case
	public static int[] mettreZeroDansLesCasesAIndicesImpair(int[] tab) {
        for (int i =1; i<tab.length; i+=2) {
            tab[i] = 0;//rends les valeurs impaires = 0
        }
        return tab;
    }
	 */

	public static int[] decalerLesElementsTableauDUneCase(int tab1[]) {
		int tab2[];
		tab2 = new int[tab1.length];
		for (int i = 0; i < tab1.length - 1; i++) {
			tab2[i + 1] = tab1[i];
		}
		tab2[0] = tab1[tab1.length - 1];
		return tab2;
	}





	public static int[] triAscendantDeuxTableaux(int[] a, int[] b) {
		int e = 0;
		int[] d = new int[a.length + b.length];
		boolean c;

		for (int i = 0; i < a.length; i++) {
			d[i] = a[i];
		}

		for (int i = 0; i < b.length; i++) {
			d[i + a.length] = b[i];
		}

		do {
			c = false;

			for (int i = 0; i < d.length - 1; i++) {
				if (d[i] > d[i + 1]) {
					e = d[i];
					d[i] = d[i + 1]; // Echangeur
					d[i + 1] = e; //
					c = true;
				}
			}
		} while (c);

		return d;
	}

	// autre technique
	/*
	public static int[] triAscendantDeuxTableaux(int[] tab1, int[] tab2) {

        int[] tabf;
        tabf = new int[tab1.length + tab2.length];

        for(int i=0; i<tab1.length;i++) {
            tabf[i] = tab1[i];
        }

        for(int i=0; i<tab2.length;i++) {
            tabf[i+tab1.length] = tab2[i];
        }

        return triAscendant(tabf);

    }
	 */


	

}












